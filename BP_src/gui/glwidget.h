/****************************************************************************
**
** Copyright (C) 2013 Digia Plc and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/legal
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Digia Plc and its Subsidiary(-ies) nor the names
**     of its contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef GLWIDGET_H
#define GLWIDGET_H

#include <sceneobject.h>
#include <objloader.h>
//#include <QGLWidget>

#include <QTcpSocket>
#include <inthread.h>

#include <misc.h>
#include <beacon.h>
#include <initdata.h>

//#define TCP_IP "147.229.13.183"
//#define TCP_PORT 12323

class GLWidget : public QGLWidget
{
    Q_OBJECT

public:
    GLWidget(InitData *ini,QWidget *parent = 0);
    ~GLWidget();

    int xRotation() const { return xRot; }
    int yRotation() const { return yRot; }
    int zRotation() const { return zRot; }

    void setMapCenter(const float lat, const float lon); //TODO public slots?, credits    
    void disableEdit();

public slots:
    void setXRotation(int angle);
    void setYRotation(int angle);
    void setZRotation(int angle);

    void onPacketReceived(QString);

signals:
    void xRotationChanged(int angle);
    void yRotationChanged(int angle);
    void zRotationChanged(int angle);
    void outPktSig(QString pkt);

protected:
    void initializeGL();
    void paintGL();
    void resizeGL(int width, int height);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);

private slots:
    void advanceGears();
    void onEdit();

private:
    void makeGround(GLuint plane);
    void getGround();
    void makeGrad();

    void normalizeAngle(int *angle);

    GLuint makeCraft(objloader plane, const GLfloat *reflectance, const GLfloat ratio, int mode);
    void drawCraft(GLdouble dx, GLdouble dy, GLdouble dz, GLuint air);
    GLuint createDesk(const GLfloat ratio);

    void LoatTxtr(int &width, int &height, QImage &buf, GLuint &texture);
    void LoadTexture(int width, int height);
    void LoadNeighbours(int width, int height);

    float rad2deg(float r);
    float deg2rad(float d);
    void posToGeo(const int x, const int y, float& lat, float& lon);
    void geoToPos(int &x, int &y, const float lat, const float lon);
    void geoToTile(int &xtile, int &ytile, const float lat, const float lon);
    void tileToGeo(int xtile, int ytile, float& lat, float& lon);
    void geoPerPix(int xtile, int ytile, float& x, float& y, float &lat_first, float &lon_first, float &lat_second, float &lon_second);
    void mouseSelectObject(int x, int y);
    void SetSelected(SceneObject *itr);
    void SetSelectedB(Beacon *itrb, bool top);
    void SetSelectedIAS(Beacon *itrb);

    void pixToGeo(int px, int py, float &x, float &y);
    void geoToPix(int &px, int &py, float x, float y);

    void rotatedPos(float &x, float &y, int px, int py);
    void unrotatedPos(int &x, int &y, int px, int py);

    void paintNormal(float alt);
    void paintPicking(float alt);

    void parseData(QString input);
    void renderIAS(float prev_x, float prev_alt, float prev_y, int i);
    void renderALT(int i);
    void paintBeacons();
    void drawInfo(QString info);
    QString getInfo();
    void paintCircle(float r);
    QString setNewCourse(float latitude, float longitude, float altitude, float b_ias);

    GLuint aircraft;
    GLuint desk;
    GLuint aircraft2;
    GLuint desk2;
    GLuint neighbours;
    objloader craft;
    objloader craft2;
    QList<SceneObject> SceneObjects;
    QList<Beacon> Beacons;

    int xRot;
    int yRot;
    int zRot;

    QPoint lastPos;

    GLuint g_texture;
    GLuint ground;
    QString tile_name;
    QImage tile_buf;
    QImage gradient;
    GLuint n_texture; //8 neighbouring tiles
    QImage n_tiles;

    int zoom, tiles;

    int pos_x, pos_y;
    int last_x, last_y;
    float latitude, longitude, heading, altitude, airspeed, vertical_speed;
    float px_x, px_y; //stupnu na pixel
    int xtile_max, ytile_max;

    int mode;

    QString pkt;
    int beacon_index;

    bool textON, infoON, editON;
    InitData *init;
};

#endif // GLWIDGET_H
