#include "objloader.h"
#include <QDir>

objloader::objloader() : id(1)
{

}

objloader::~objloader()
{
}

int objloader::load(char *filename)
{    
    fstream objFile;
    //QDir dir;
    //QString fn = QString(filename);
    //fn = dir.absoluteFilePath(fn);
    //fn = dir.toNativeSeparators(fn);
    //qDebug() << fn;
//    QString name = dir.absoluteFilePath(fn);

    objFile.open(/*fn.toStdString().c_str());*/filename);

    if(objFile.is_open())
    {
        char line[255];

        // Parse object file line by line
        while(objFile.good())
        {
            objFile.getline(line, 255);
            parseLine(line);
        }

        objFile.close();
    }
    else
    {
        cout << "Could not open objloader file '" << filename << "'\n";
        return false;
    }
    return true;
}

void objloader::parseLine(char *line)
{
    if(!strlen(line))
    {
        return;
    }

    char *lineType;
    lineType = strtok(strdup(line), " ");

    // Decide what to do
    if(!strcmp(lineType, "v"))		// Vertex
    {
        parseVertex(line);
    }
    else if(!strcmp(lineType, "vn"))	// Normal
    {
        parseNormal(line);
    }
    else if(!strcmp(lineType, "f"))	// Face
    {
        parseFace(line);
    }
    return;
}

// Draw object
void objloader::draw()
{
    if (faces[0].vn1 == 0.0f) {
        this->drawVertices2();
        return;
    }
    glBegin(GL_TRIANGLES);

    for(unsigned f = 0; f < faces.size(); f++)
    {
        glNormal3f(normals[faces[f].vn1 - 1].x, normals[faces[f].vn1 - 1].y, normals[faces[f].vn1 - 1].z);
        glVertex3f(vertices[faces[f].v1 - 1].x, vertices[faces[f].v1 - 1].y, vertices[faces[f].v1 - 1].z);

        glNormal3f(normals[faces[f].vn2 - 1].x, normals[faces[f].vn2 - 1].y, normals[faces[f].vn2 - 1].z);
        glVertex3f(vertices[faces[f].v2 - 1].x, vertices[faces[f].v2 - 1].y, vertices[faces[f].v2 - 1].z);

        glNormal3f(normals[faces[f].vn3 - 1].x, normals[faces[f].vn3 - 1].y, normals[faces[f].vn3 - 1].z);
        glVertex3f(vertices[faces[f].v3 - 1].x, vertices[faces[f].v3 - 1].y, vertices[faces[f].v3 - 1].z);
    }

    glEnd();
}

// Draw object
void objloader::drawVertices()
{
    glBegin(GL_TRIANGLES);

    for(unsigned f = 0; f < faces.size(); f++)
    {
        glVertex3f(vertices[faces[f].v1 - 1].x, vertices[faces[f].v1 - 1].y, vertices[faces[f].v1 - 1].z);

        glVertex3f(vertices[faces[f].v2 - 1].x, vertices[faces[f].v2 - 1].y, vertices[faces[f].v2 - 1].z);

        glVertex3f(vertices[faces[f].v3 - 1].x, vertices[faces[f].v3 - 1].y, vertices[faces[f].v3 - 1].z);
    }

    glEnd();
}

void objloader::drawVertices2()
{

    glBegin(GL_TRIANGLES);

    for(unsigned f = 0; f < faces.size(); f++)
    {
        glNormal3f(vertices[faces[f].v1 - 1].x, vertices[faces[f].v1 - 1].y, vertices[faces[f].v1 - 1].z);
        glVertex3f(vertices[faces[f].v1 - 1].x, vertices[faces[f].v1 - 1].y, vertices[faces[f].v1 - 1].z);

        glNormal3f(vertices[faces[f].v2 - 1].x, vertices[faces[f].v2 - 1].y, vertices[faces[f].v2 - 1].z);
        glVertex3f(vertices[faces[f].v2 - 1].x, vertices[faces[f].v2 - 1].y, vertices[faces[f].v2 - 1].z);

        glNormal3f(vertices[faces[f].v3 - 1].x, vertices[faces[f].v3 - 1].y, vertices[faces[f].v3 - 1].z);
        glVertex3f(vertices[faces[f].v3 - 1].x, vertices[faces[f].v3 - 1].y, vertices[faces[f].v3 - 1].z);
    }

    glEnd();
}

// Parse a "v" vertex line of the file into our vertices array
void objloader::parseVertex(char *line)
{

    QString s = QString(line);
    QStringList list = s.split(" ");
    vertices.push_back((Vector) {list[1].toFloat(),list[2].toFloat(),list[3].toFloat()});

    return;
}

// Parse a "vn" normal line of the file into the normals array
void objloader::parseNormal(char *line)
{
    QString s = QString(line);
    QStringList list = s.split(" ");
    normals.push_back((Vector) {list[1].toFloat(),list[2].toFloat(),list[3].toFloat()});

    return;
}

// Parse a "f" face line into the faces array. This gets complex due to there being different line formats.
void objloader::parseFace(char *line)
{
   /* int fill = 0;
    faces.push_back(Face());

*/
    // Read face line. If texture indicies aren't present, don't read them.
    /*if(sscanf(line, "f %d//%d %d//%d %d//%d", &faces.back().v1,
                                              &faces.back().vn1,
                                              &faces.back().v2,
                                              &faces.back().vn2,
                                              &faces.back().v3,
                                              &faces.back().vn3) <= 1)
    {
        sscanf(line, "f %d/%d/%d %d/%d/%d %d/%d/%d", &faces.back().v1,
                                                     &fill,
                                                     &faces.back().vn1,
                                                     &faces.back().v2,
                                                     &fill,
                                                     &faces.back().vn2,
                                                     &faces.back().v3,
                                                     &fill,
                                                     &faces.back().vn3);
    }*/
    Face elem;

    QString s = QString(line);
    QStringList list = s.split(QRegExp("\\W+"));

    if (list.size() > 9) {
        elem.v1 = list[1].toInt();
        elem.vn1 = list[3].toFloat();
        elem.v2 = list[4].toInt();
        elem.vn2 = list[6].toFloat();
        elem.v3 = list[7].toInt();
        elem.vn3 = list[9].toFloat();
    }
    else if (list.size() > 6){
        elem.v1 = list[1].toInt();
        elem.vn1 = list[2].toFloat();
        elem.v2 = list[3].toInt();
        elem.vn2 = list[4].toFloat();
        elem.v3 = list[5].toInt();
        elem.vn3 = list[6].toFloat();
    }
    else if (list.size() == 4) {
        elem.v1 = list[1].toInt();
        elem.vn1 = 0.0f;
        elem.v2 = list[2].toInt();
        elem.vn2 = 0.0f;
        elem.v3 = list[3].toInt();
        elem.vn3 = 0.0f;
    }
    faces.push_back(elem);

    return;
}

void objloader::getFaces(vector<Face>& f, vector<Vector>& v, vector<Vector>& n)
{
    f = faces;
    v = vertices;
    n = normals;
}
