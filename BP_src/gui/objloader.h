//http://jamwaffles.co.uk/tutorials/opengl/wavefrontloader/cpp

#ifndef OBJLOADER_H
#define OBJLOADER_H

#include <iostream>
#include <fstream>
#include <vector>
#include <cstring>
using namespace std;

#include <QtWidgets>
#include <QtOpenGL>

#define GLBUFFERS_ASSERT_OPENGL(prefix, assertion, returnStatement)                         \
if (m_failed || !(assertion)) {                                                             \
    if (!m_failed) qCritical(prefix ": The necessary OpenGL functions are not available."); \
    m_failed = true;                                                                        \
    returnStatement;                                                                        \
}

typedef struct
{
    float x;
    float y;
    float z;
}
Vector;

typedef struct
{
    int v1, v2, v3;
    int vn1, vn2, vn3;
}
Face;

class objloader
{
private:
    // Dynamic variables to keep our object data in
    vector<Vector> vertices;
    vector<Vector> normals;
    vector<Face> faces;

    void parseLine(char *line);

    void parseVertex(char *line);
    void parseNormal(char *line);
    void parseFace(char *line);

protected:
    GLuint id;
    bool m_failed;
public:
    objloader();
    ~objloader();

    int load(char *filename);
    void draw();
    void drawVertices();
    void drawVertices2();
    void getFaces(vector<Face>& f, vector<Vector>& v, vector<Vector>& n);
};

#endif // OBJLOADER_H
