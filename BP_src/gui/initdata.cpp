#include "initdata.h"

InitData::InitData()
{
    tcp_ip = "127.0.0.1"; //"147.229.13.183"
    tcp_port = 12321;
    max_alt = 11278;
    min_alt = 600;
    max_speed = 1.0;
    min_speed = 0.2;
    aircraft_model = "./models/Boeing737/Boeing737.obj";
    aircraft_model_ratio = 0.04f;
    beacon_model = "./models/dual_pyramid.obj";
    beacon_model_ratio = 0.7f;
    zoom = 9;

    std::fstream csvFile;
    csvFile.open("init.csv");

    if(csvFile.is_open())
    {
        char line[255];

        // Parse object file line by line
        while(csvFile.good())
        {
            csvFile.getline(line, 255);
            parseLine(line);
        }

        csvFile.close();
    }
    range_alt = max_alt - min_alt;
}

void InitData::parseLine(char *line)
{
    QString str(line);
    str = str.simplified();
    str.remove(" ");
    QStringList items = str.split(",", QString::SkipEmptyParts);
    if (!str.isEmpty() && !items[0].isEmpty()) {
        if (items[0] == "tcp_ip") tcp_ip = items[1].remove('"');
        else if (items[0] == "tcp_port") tcp_port = items[1].toInt();
        else if (items[0] == "max_alt")  max_alt = items[1].toInt();
        else if (items[0] == "min_alt")  min_alt = items[1].toInt();
        else if (items[0] == "max_speed")  max_speed = items[1].toFloat();
        else if (items[0] == "min_speed")  min_speed = items[1].toFloat();
        else if (items[0] == "aircraft_model")  aircraft_model = items[1].remove('"');
        else if (items[0] == "beacon_model")  beacon_model = items[1].remove('"');
        else if (items[0] == "zoom") zoom = items[1].toInt();
        else if (items[0] == "aircraft_model_ratio") aircraft_model_ratio = items[1].toFloat();
        else if (items[0] == "beacon_model_ratio") beacon_model_ratio = items[1].toFloat();
    }
}
