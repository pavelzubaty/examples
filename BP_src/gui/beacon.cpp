#include "beacon.h"

Beacon::Beacon(QString n, ParsedData data, int in, InitData *ini)
{
    init = ini;
    index = in;
    index++;
    name = n;
    //name.append(QString::number(index));
    latitude = data.lat;
    longitude = data.lon;
    altitude = data.alt;
    airspeed = data.ias;

    prev_x = 0.0f;
    prev_alt = altitude;
    prev_y = 12.0f;

    int r,g,b;
    intToPixel(index, r, g, b);
    m_colorID[0] = (unsigned char) r;
    m_colorID[1] = (unsigned char) g;
    m_colorID[2] = (unsigned char) b;

    float ratio = init->beacon_model_ratio; //0.7;

    QString file_name = init->beacon_model;
    QByteArray arr = file_name.toLocal8Bit();
    char *str = arr.data();
    int i = vertical_mark.load(str);

    if (!i) {
        cerr << "Selhalo nacteni modelu znacky" << file_name.toStdString();
        return;
    }

    vector<Vector> ver;
    vector<Vector> nor;
    vector<Face> fac;

    vertical_mark.getFaces(fac,ver,nor);
    GLuint list = glGenLists(1);
    glNewList(list, GL_COMPILE);
    static const GLfloat reflectance[4] = { 0.0f, 0.8f, 0.0f, 1.0f };
    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, reflectance);
    glShadeModel(GL_SMOOTH_POINT_SIZE_RANGE);
    glScalef(ratio,ratio,ratio);

    vertical_mark.draw();

    glEndList();

    v_mark = list;

    createMapMark();
}

void Beacon::createMapMark() {
    GLuint list = glGenLists(1);
    glNewList(list, GL_COMPILE);
        static const GLfloat outerr[4] = { 0.0f, 0.0f, 1.0f, 0.6f }; //TODO barva dle ALT
        static const GLfloat gapr[4] = { 0.0f, 0.0f, 1.0f, 0.15f };
        static const GLfloat innerr[4] = { 0.0f, 0.0f, 0.0f, 0.5f };
        const float ringw = 0.3f;
        const float r = 1.2f;
        float ratio = init->beacon_model_ratio;

        glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, innerr);
        DrawCircle(0.0f,0.0f,2.0f,0.5f,12);
        glTranslatef(0.0f,-0.01f,0.0f);
        glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, gapr);
        DrawCircle(0.0f,0.0f,2.0,r-ringw,12);
        glTranslatef(0.0f,-0.01f,0.0f);
        glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, outerr);
        DrawCircle(0.0f,0.0f,2.0f,r,12);
        glTranslatef(0.0f,0.7f,0.0f);
        glScalef(0.8f*ratio,1.0f*ratio,0.8f*ratio);
        vertical_mark.draw();
    glEndList();    
    m_mark = list;
}

void Beacon::draw(int x, int y, int rot) {
    yRot = -1*rot/16.0f;
    float tx,ty;
    rotated(tx,ty,x,y,yRot);

    xx = tx;
    alt = 2.0 + altitude /(init->range_alt/14);
    px = (x - (TILE_WIDTH/2)) * 24.0/TILE_WIDTH;
    py = (y - (TILE_HEIGHT/2)) * 24.0/TILE_HEIGHT;
    static const GLfloat refl[4] = { 1.0f, 1.0f, 1.0f, 0.8f }; //0.3f

    glPushMatrix();
        glTranslated(px, alt, py);
        glCallList(v_mark);
    glPopMatrix();

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable( GL_LINE_SMOOTH );
    glEnable(GL_DEPTH_TEST);

    glPushMatrix();
        glTranslated(px, 0.0f, py);
        glCallList(m_mark);
    glPopMatrix();    

    glPushMatrix();
        glTranslated(px, alt, py);
        DrawLine (0.0f, -alt, 0.0f, 0.0f, 0.0f, 0.0f,refl);
    glPopMatrix();
}

void Beacon::picking() {
    float arr_alt = (alt + prev_alt)/4.0f + 1.0f;

    // set mesh position
    glPushMatrix();

        glColor3f(m_colorID[2]/255.0f, m_colorID[1]/255.0f, 255.0f);
        glPushMatrix();
            DrawArrow (prev_x+0.01f, arr_alt, prev_y+0.01f, px+0.01f, py+0.01f, airspeed/MAX_SPEED);
        glPopMatrix();

        glColor3f(m_colorID[0]/255.0f, m_colorID[1]/255.0f, m_colorID[2]/255.0f);

        glPushMatrix();
            glTranslated(px, 0.0f, py);
            glCallList(m_mark);
            DrawCircle(0.0f,0.0f,0.0f,4.0f,12);
        glPopMatrix();

        glColor3f(m_colorID[2]/255.0f, m_colorID[1]/255.0f, m_colorID[2]/255.0f); //mark the vertical mark with different color
        glPushMatrix();
            glTranslated(px, alt, py);
            glScalef(2.0f,2.0f,2.0f);
            glCallList(v_mark);
        glPopMatrix();

    glPopMatrix();
}

void Beacon::changeAlt(int dy) {
    altitude = altitude - dy*(init->range_alt/TILE_HEIGHT);
    altitude = (altitude < init->min_alt) ? init->min_alt : altitude;
    altitude = (altitude > init->max_alt) ? init->max_alt : altitude;
}


void Beacon::changeMap(float x, float y) {
    longitude = x;
    latitude = y;
}

void Beacon::setNewPos(int dx, int dy) {
    px_dx = dx;
    px_dy = dy;
}

void Beacon::changeIAS(int dx, int dy, float prev_xx) {
    float signx = (dx*(xx-prev_xx) > 0.0f) ? 1.0f : -1.0f;
    float dd = sqrt (dx*dx + dy*dy);
    airspeed += signx*dd*0.01f;
    airspeed = (airspeed < init->min_speed) ? init->min_speed : ((airspeed > init->max_speed) ? init->max_speed : airspeed);
}

void Beacon::drawLine(float pre_x, float pre_alt, float pre_y,const GLfloat *reflectance) {
    glPushMatrix();
        glNormal3f(0.0f,1.0f,0.0f);
        DrawLine (px, alt, py,pre_x, pre_alt, pre_y,reflectance);
        prev_x = pre_x;
        prev_alt = pre_alt;
        prev_y = pre_y;
    glPopMatrix();
}

void Beacon::drawConnection(const GLfloat *reflectance,const GLfloat *reflectance2) {
    float p_alt = prev_alt+0.1f;
    float c_alt = alt + 0.1f;
    float arr_alt = (alt + prev_alt)/4 + 1.0f;
    glPushMatrix();
    glDepthMask(false); //disable z-testing
        glBegin(GL_TRIANGLES);
            glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, reflectance);
            glNormal3f(prev_x,p_alt,prev_y);
            glVertex3f(prev_x,p_alt,prev_y);
            glNormal3f(prev_x,0.0f,prev_y);
            glVertex3f(prev_x,0.0f,prev_y);
            glNormal3f(px,0.0f,py);
            glVertex3f(px,0.0f,py);

            //glNormal3f(px,0.0f,py);
            glVertex3f(px,0.0f,py);
            glNormal3f(px,c_alt,py);
            glVertex3f(px,c_alt,py);
            glNormal3f(prev_x,p_alt,prev_y);
            glVertex3f(prev_x,p_alt,prev_y);
        glEnd();

        glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, reflectance2);
        DrawArrow (prev_x+0.01f, arr_alt, prev_y+0.01f, px+0.01f, py+0.01f, airspeed/init->max_speed);

    glDepthMask(true); //enable z-testing (for the next frame)
    glPopMatrix();
}
