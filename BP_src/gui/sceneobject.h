#ifndef SCENEOBJECT_H
#define SCENEOBJECT_H

#include <QtWidgets>
#include <QtOpenGL>

//http://content.gpwiki.org/index.php/OpenGL_Selection_Using_Unique_Color_IDs
class BaseObject
 {
 public:
      unsigned char m_colorID[3];
      static unsigned char gColorID[3];

      BaseObject();

      ~BaseObject();
};

class SceneObject //: public BaseObject
{
private:
     float m_position[3];
     QString m_name;
     GLuint obj; //list of vertices

public:
     unsigned char m_colorID[4];
     SceneObject(unsigned char colorID[3],QString name, GLuint vertices) ;
     ~SceneObject();

     void Init(QString name, GLuint vertices);
     void Picking();
     QString getName() const {return m_name; }
};

#endif // SCENEOBJECT_H
