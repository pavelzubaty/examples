/****************************************************************************
**
** Copyright (C) 2013 Digia Plc and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/legal
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Digia Plc and its Subsidiary(-ies) nor the names
**     of its contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "glwidget.h"
#include "mainwindow.h"

#include <QtWidgets>
/*
#include <QTcpSocket>

#define TCP_IP "147.229.13.183"
#define TCP_PORT 12323
*/
MainWindow::MainWindow()
{
    InitData *init = new InitData();

    this->setCursor(Qt::PointingHandCursor);

    centralWidget = new QWidget;
    setCentralWidget(centralWidget);

    glWidget = new GLWidget(init);

    edit_button = new QPushButton(tr("EDIT"),this);
    edit_button->setFixedSize(QSize(80,48));
    edit_button->setStyleSheet("background-color: qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 #5c5c5c, stop:1 #1a1a1a); color:#FFFFFF;");
    edit_button->move(490,10);
    edit_button->setFont(QFont("Arial",20,QFont::Normal));
    //    edit_button->setGeometry(QRect(QPoint(530,0),QSize(50,20)));
    connect(edit_button,SIGNAL(pressed()),glWidget,SLOT(onEdit()));
    connect(edit_button,SIGNAL(pressed()),this,SLOT(setEdit()));

    pixmapLabel = new QLabel;

    glWidgetArea = new QScrollArea;
    glWidgetArea->setWidget(glWidget);
    glWidgetArea->setWidgetResizable(true);
    glWidgetArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    glWidgetArea->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    glWidgetArea->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
    glWidgetArea->setMinimumSize(50, 50);
/*
    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(startThread()));
    timer->start(50);
    */

    nThread = new inThread(this);
    nThread->init(init->tcp_ip, init->tcp_port);
    connect(nThread, SIGNAL(packetReceived(QString)), glWidget, SLOT(onPacketReceived(QString)));
    connect(glWidget,SIGNAL(outPktSig(QString)), nThread, SLOT(onPacketSending(QString)));
    nThread->start();    
    connect(nThread, SIGNAL(finished()), nThread, SLOT(deleteLater()));

/*
    pixmapLabelArea = new QScrollArea;
    pixmapLabelArea->setWidget(pixmapLabel);
    pixmapLabelArea->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
    pixmapLabelArea->setMinimumSize(50, 50);
*/
    xSlider = createSlider(SIGNAL(xRotationChanged(int)),
                           SLOT(setXRotation(int)));
    ySlider = createSlider(SIGNAL(yRotationChanged(int)),
                           SLOT(setYRotation(int)));
    zSlider = createSlider(SIGNAL(zRotationChanged(int)),
                           SLOT(setZRotation(int)));

    xSlider->setStyleSheet("QSlider::groove:horizontal {"
                           "border: none;"
                           "background: #3F3F3F;"
                           "height: 2px;"
                           "margin: 0 0 0 0;}"
                           "QSlider::handle:horizontal {"
                           "background: qlineargradient(x1:0, y1:0, x2:1, y2:1, stop:0 #5c5c5c, stop:1 #1a1a1a);"
                           "border: 1px solid #000000;"
                           "width: 50px;"
                           "margin: -5px 0 -5px 0;"
                           "border-radius: 0px;}");
    ySlider->setStyleSheet("QSlider::groove:horizontal {"
                           "border: none;"
                           "background: #3F3F3F;"
                           "height: 2px;"
                           "margin: 0 0 0 0;}"
                           "QSlider::handle:horizontal {"
                           "background: qlineargradient(x1:0, y1:0, x2:1, y2:1, stop:0 #5c5c5c, stop:1 #1a1a1a);"
                           "border: 1px solid #000000;"
                           "width: 50px;"
                           "margin: -5px 0 -5px 0;"
                           "border-radius: 0px;}");
    createActions();
    //createMenus();

    QGridLayout *centralLayout = new QGridLayout;
    centralLayout->addWidget(glWidgetArea, 0, 0);
    centralLayout->addWidget(xSlider, 1, 0, 1, 1);
    centralLayout->addWidget(ySlider, 2, 0, 1, 1);
    //centralLayout->addWidget(zSlider, 3, 0, 1, 1);
    centralWidget->setLayout(centralLayout);
    centralWidget->setFixedSize(590,600);
    centralWidget->layout()->setContentsMargins(0,0,0,0);
    centralWidget->setStyleSheet("background: #0F0F0F;");
/*
    centralLayout->addWidget(pixmapLabelArea, 0, 1);
    centralLayout->addWidget(xSlider, 1, 0, 1, 2);
    centralLayout->addWidget(ySlider, 2, 0, 1, 2);
    centralLayout->addWidget(zSlider, 3, 0, 1, 2);
    centralWidget->setLayout(centralLayout);
*/
    xSlider->setValue(30 * 16);
    ySlider->setValue(345 * 16);
    zSlider->setValue(0 * 16);

    setWindowTitle(tr("Autopilot User Interface"));
    //resize(580, 580);
    resize(590, 600);
    setFixedSize(590, 600);
/*
    QString str = this->getData();
    ParsedData data = this->parseData(str);
    qDebug() << data.lon << data.lat << data.hea << data.alt << data.ias << data.vs;*/
}

void MainWindow::renderIntoPixmap()
{
    QSize size = getSize();
    if (size.isValid()) {
        QPixmap pixmap = glWidget->renderPixmap(size.width(), size.height());
        setPixmap(pixmap);
    }
}

void MainWindow::grabFrameBuffer()
{
    QImage image = glWidget->grabFrameBuffer();
    setPixmap(QPixmap::fromImage(image));
}

void MainWindow::clearPixmap()
{
    setPixmap(QPixmap());
}

void MainWindow::about()
{
    QMessageBox::about(this, tr("About"),
            tr("This program sets course of the autopilot."));
}

void MainWindow::createActions()
{
    /*
    renderIntoPixmapAct = new QAction(tr("&Render into Pixmap..."), this);
    renderIntoPixmapAct->setShortcut(tr("Ctrl+R"));
    connect(renderIntoPixmapAct, SIGNAL(triggered()),
            this, SLOT(renderIntoPixmap()));

    grabFrameBufferAct = new QAction(tr("&Grab Frame Buffer"), this);
    grabFrameBufferAct->setShortcut(tr("Ctrl+G"));
    connect(grabFrameBufferAct, SIGNAL(triggered()),
            this, SLOT(grabFrameBuffer()));

    clearPixmapAct = new QAction(tr("&Clear Pixmap"), this);
    clearPixmapAct->setShortcut(tr("Ctrl+L"));
    connect(clearPixmapAct, SIGNAL(triggered()), this, SLOT(clearPixmap()));
*/
    exitAct = new QAction(tr("E&xit"), this);
    exitAct->setShortcuts(QKeySequence::Quit);
    connect(exitAct, SIGNAL(triggered()), this, SLOT(close()));

    aboutAct = new QAction(tr("&About"), this);
    connect(aboutAct, SIGNAL(triggered()), this, SLOT(about()));

    aboutQtAct = new QAction(tr("About &Qt"), this);
    connect(aboutQtAct, SIGNAL(triggered()), qApp, SLOT(aboutQt()));
}

void MainWindow::createMenus()
{
    fileMenu = menuBar()->addMenu(tr("&File"));
    fileMenu->addAction(renderIntoPixmapAct);
    fileMenu->addAction(grabFrameBufferAct);
    fileMenu->addAction(clearPixmapAct);
    fileMenu->addSeparator();
    fileMenu->addAction(exitAct);

    helpMenu = menuBar()->addMenu(tr("&Help"));
    helpMenu->addAction(aboutAct);
    helpMenu->addAction(aboutQtAct);
}

QSlider *MainWindow::createSlider(const char *changedSignal,
                                  const char *setterSlot)
{
    QSlider *slider = new QSlider(Qt::Horizontal);
    slider->setRange(0, 360 * 16);
    slider->setSingleStep(16);
    slider->setPageStep(15 * 16);
    slider->setTickInterval(15 * 16);
    slider->setTickPosition(QSlider::TicksRight);
    connect(slider, SIGNAL(valueChanged(int)), glWidget, setterSlot);
    connect(glWidget, changedSignal, slider, SLOT(setValue(int)));
    return slider;
}

void MainWindow::setPixmap(const QPixmap &pixmap)
{
    pixmapLabel->setPixmap(pixmap);
    QSize size = pixmap.size() / pixmap.devicePixelRatio();
    if (size - QSize(1, 0) == pixmapLabelArea->maximumViewportSize())
        size -= QSize(1, 0);
    pixmapLabel->resize(size);
}

void MainWindow::startThread()
{
    nThread = new inThread(this);
    connect(nThread, SIGNAL(packetReceived(QString)), glWidget, SLOT(onPacketReceived(QString)));
    connect(glWidget,SIGNAL(outPktSig(QString)), nThread, SLOT(onPacketSending(QString)));
    nThread->start();
    connect(nThread, SIGNAL(finished()), nThread, SLOT(deleteLater()));
}

void MainWindow::setEdit()
{
    if (this->edit_txt == "VIEW") {
        this->edit_txt = "EDIT";
        edit_button->setStyleSheet("background-color: qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 #5c5c5c, stop:1 #1a1a1a); color:#FFFFFF;");
    }
    else {
        this->edit_txt = "VIEW";
        edit_button->setStyleSheet("background-color: qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 #00FF00, stop:1 #00C400); color:#000000;");
        //TODO: timer podle aktivity v glWidget (prodluzovani periody timeru podle akci)
        /*
        timer = new QTimer(this);
        connect(timer, SIGNAL(timeout()), this, SLOT(disableEdit()));
        timer->start(10*1000);*/
    }
    edit_button->setText(tr("EDIT")); //(this->edit_txt.toStdString().c_str()));
}

void MainWindow::disableEdit()
{
    qDebug() << "timeout";
    this->edit_txt = "EDIT";
    this->edit_button->setText(tr("EDIT"));
    glWidget->disableEdit();
    delete timer;
}

QSize MainWindow::getSize()
{
    bool ok;
    QString text = QInputDialog::getText(this, tr("Autopilot User Interface"),
                                         tr("Enter pixmap size:"),
                                         QLineEdit::Normal,
                                         tr("%1 x %2").arg(glWidget->width())
                                                      .arg(glWidget->height()),
                                         &ok);
    if (!ok)
        return QSize();

    QRegExp regExp(tr("([0-9]+) *x *([0-9]+)"));
    if (regExp.exactMatch(text)) {
        int width = regExp.cap(1).toInt();
        int height = regExp.cap(2).toInt();
        if (width > 0 && width < 2048 && height > 0 && height < 2048)
            return QSize(width, height);
    }

    return glWidget->size();
}

MainWindow::~MainWindow() {
    nThread->endflag = 1;
    nThread->exit();
    nThread->terminate();
    nThread->wait(50);
}
