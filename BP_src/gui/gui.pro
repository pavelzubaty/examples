HEADERS       = glwidget.h \
                mainwindow.h \
    objloader.h \
    sceneobject.h \
    inthread.h \
    beacon.h \
    misc.h \
    initdata.h
SOURCES       = glwidget.cpp \
                main.cpp \
                mainwindow.cpp \
    objloader.cpp \
    sceneobject.cpp \
    inthread.cpp \
    beacon.cpp \
    misc.cpp \
    initdata.cpp
QT           += opengl widgets gui network

# install
#target.path = $$[QT_INSTALL_EXAMPLES]/opengl/grabber
target.path = /home/paulzy/bakalarka/autopilot/gui/target
INSTALLS += target

contains(QT_CONFIG, opengles.) {
    contains(QT_CONFIG, angle): \
        warning("Qt was built with ANGLE, which provides only OpenGL ES 2.0 on top of DirectX 9.0c")
    error("This example requires Qt to be configured with -opengl desktop")
}

