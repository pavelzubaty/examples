/****************************************************************************
**
** Copyright (C) 2013 Digia Plc and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/legal
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Digia Plc and its Subsidiary(-ies) nor the names
**     of its contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "glwidget.h"

#include <QMouseEvent>
#include <QTimer>

#include <math.h>
#include <iostream>

#include <QDebug>

#include <objloader.h>
//#include <GL/glut.h>

GLWidget::GLWidget(InitData *ini, QWidget *parent)
    : QGLWidget(parent)
{
    textON = true;
    infoON = true;
    editON = false;

    init = ini; //init.csv data

    xRot = 0;
    yRot = 0;
    zRot = 0;
    aircraft = 0;

    ground = 0;
    g_texture = 0;
    n_texture = 0;

    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(advanceGears()));
    timer->start(50); //20

    tile_name ="/home/paulzy/.local/share/Maps/OSM/12/2237/1404.png";
    tile_buf.load("/home/paulzy/.local/share/Maps/OSM/12/2237/1404.png");

    tile_buf = QImage(TILE_WIDTH,TILE_HEIGHT,QImage::Format_ARGB32); //tile_buf.convertToFormat(QImage::Format_ARGB32);

    zoom = init->zoom; //9;
    tiles = 1 << zoom;
    pos_x = pos_y = 0;
    latitude = 49.154036; //49.268799;
    longitude = 16.692649; //17.843283;
    heading = 90.0;
    altitude = 11000;
    airspeed = 170; //mps

    px_x = 180.0f / (TILE_WIDTH * pow(2.0,zoom));
    px_y = 180.0f / (TILE_HEIGHT * pow(2.0,zoom));
    xtile_max = pow(2,zoom);
    ytile_max = pow(2,zoom);
    mode = DRAW;
    gradient = QImage(TILE_WIDTH,TILE_HEIGHT,QImage::Format_ARGB32);
    if (!gradient.load("gradient.png"))
        gradient = createGradient(TILE_WIDTH,TILE_HEIGHT);
    beacon_index = 0;
}

GLWidget::~GLWidget()
{
    makeCurrent();
}

void GLWidget::setXRotation(int angle)
{
    normalizeAngle(&angle);
    if (angle != xRot) {
        xRot = angle;
        emit xRotationChanged(angle);
        updateGL();
    }
}

void GLWidget::setYRotation(int angle)
{
    normalizeAngle(&angle);
    if (angle != yRot) {
        yRot = angle;
        emit yRotationChanged(angle);
        updateGL();
    }
}

void GLWidget::setZRotation(int angle)
{
    normalizeAngle(&angle);
    if (angle != zRot) {
        zRot = angle;
        emit zRotationChanged(angle);
        updateGL();
    }
}

void GLWidget::initializeGL()
{
    static const GLfloat lightPos[4] = {0.0f, 0.0f, 10.0f, 0.0f};
    static const GLfloat reflectance1[4] = { 0.8f, 0.1f, 0.0f, 1.0f };
    //const GLfloat ratio = 0.04;
    const GLfloat ratio2 = 12.0;
    const GLfloat ratio3 = 24.0;

    aircraft2 = makeCraft(craft2,reflectance1,init->aircraft_model_ratio,SELECT);
    desk2 = createDesk(ratio2 - 0.1); //edge gradient fix    

    glLightfv(GL_LIGHT0, GL_POSITION, lightPos);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_DEPTH_TEST);
    aircraft = makeCraft(craft,reflectance1,init->aircraft_model_ratio,DRAW);

    desk = createDesk(ratio2);
    neighbours = createDesk(ratio3);

    glShadeModel(GL_SMOOTH);
    glEnable(GL_NORMALIZE);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

}

void GLWidget::paintCircle(float r)
{
    glPushMatrix();
    glTranslatef(0.0f,0.1f,12.0f);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable( GL_LINE_SMOOTH );
    static const GLfloat reflecta[4] = { 0.5f, 0.5f, 0.5f, 0.7f };
    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, reflecta);
    glLineWidth( 1.0 );
    DrawCircleEmpty(0.0f,0.0f,0.0f,r,36); //TODO draw pixels only
    glPopMatrix();
}

void GLWidget::paintNormal(float alt)
{
    makeGround(neighbours);
    glTranslatef(0.0,0.1,0.0);
    makeGround(desk);

    float r = 24.0f/(LAT_KM*px_x * TILE_WIDTH); //for reference purposes only
    paintCircle(r*75);
    paintCircle(r*50);
    paintCircle(r*25);
    paintCircle(r*12.5);

    glTranslatef(0.0,alt,0.0);
    drawCraft(0.0, 0.0, 12.0,aircraft);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable( GL_LINE_SMOOTH );
    glPushMatrix();
        static const GLfloat refl[4] = { 1.0f, 1.0f, 1.0f, 0.8f };
        glTranslatef(0.0f,0.0f,12.0f);
        DrawLine (0.0f, -alt, 0.0f, 0.0f, 0.0f, 0.0f,refl);
    glPopMatrix();
    glTranslatef(0.0,-alt,0.0);
    paintBeacons();
}

void GLWidget::renderIAS(float prev_x, float prev_alt, float prev_y, int i)
{
    glPushMatrix();
        glDisable(GL_LIGHTING);
        glDisable(GL_DEPTH_TEST);
            glColor3f(1.0f, 1.0f, 1.0f);
            QString ias = QString::number(Beacons[i].Airspeed()).left(4).append("M");
            double ppx = (Beacons[i].Px() + prev_x)/2;
            double ppy = (Beacons[i].Py() + prev_y)/2;
            double palt = (Beacons[i].Alt() + prev_alt)/2;
            renderText(ppx, palt/2, ppy, ias, QFont("Arial", 14, QFont::Normal, false) );
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_LIGHTING);
    glPopMatrix();
}

void GLWidget::renderALT(int i)
{
    glPushMatrix();
        glDisable(GL_LIGHTING);
        glDisable(GL_DEPTH_TEST);
            glColor3f(1.0f, 1.0f, 1.0f);
            QString alt = QString::number((int)Beacons[i].Altitude()).append("ft");
            double ppx = Beacons[i].Px()*1.2;
            double ppy = Beacons[i].Py();
            double palt = Beacons[i].Alt();
            renderText(ppx, palt, ppy, alt, QFont("Arial", 10, QFont::Normal, false) );
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_LIGHTING);
    glPopMatrix();
}

void GLWidget::paintBeacons()
{
    //remove the first if too close
    while (!Beacons.isEmpty())
    {
        if (fabs(Beacons[0].Latitude() - this->latitude) < DISTANCE_MIN
            && fabs(Beacons[0].Longitude() - this->longitude) < DISTANCE_MIN)
        {
            Beacons.removeFirst();
            for(int i=0; i < Beacons.size(); i++) {
                Beacons[i].index = i+1;
            }

            if (this->beacon_index == 0) {
                mode = (mode == BEACON_ALT || mode == BEACON_MAP || mode == GET_BEACON_PIXEL || mode == BEACON_IAS) ? DRAW : mode;
            }
        }
        else {
            break;
        }
    }

    //TODO: SET new course according to the first beacon
    if (!Beacons.isEmpty()) {
        QString pkt_out = setNewCourse(Beacons[0].Latitude(),Beacons[0].Longitude(),Beacons[0].Altitude(), Beacons[0].Airspeed());
        emit(outPktSig(pkt_out));
    }

    int x,y,px,py;
    float prev_x, prev_y, prev_alt;
    //geoToPix(px, py, this->longitude, this->latitude);
    //unrotatedPos(x,y,px,py);
    prev_x = 0.0f;
    prev_y = 12.0f;
    prev_alt =  2.0 + altitude/(init->range_alt/14);
    static const GLfloat refl[4] = { 0.0f, 1.0f, 1.0f, 0.95f }; //0.3f
    static const GLfloat refl2[4] = { 0.0f, 1.0f, 0.0f, 0.25f };
    static const GLfloat refl3[4] = { 1.0f, 1.0f, 1.0f, 0.9f };
    for (int i=0; i< Beacons.size(); i++) {
        geoToPix(px, py, Beacons[i].Longitude(), Beacons[i].Latitude());
        unrotatedPos(x,y,px,py);

        if (x < 1.3*TILE_WIDTH && y < 1.3*TILE_HEIGHT) {

            Beacons[i].draw(x,y,yRot);

            Beacons[i].drawLine(prev_x,prev_alt,prev_y,refl);

            Beacons[i].drawConnection(refl2,refl3);

            if (textON) {
                renderALT(i);
                renderIAS(prev_x, prev_alt, prev_y, i);
            }

            prev_x = Beacons[i].Px();
            prev_y = Beacons[i].Py();
            prev_alt = Beacons[i].Alt();
        }        
    }
}

void GLWidget::paintPicking(float alt)
{
    glDisable(GL_LIGHTING);
    glDisable(GL_LIGHT0);
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_TEXTURE_2D);

    SceneObjects.clear();
    unsigned char colorID[3];
    colorID[0] = 1;
    colorID[1] = 0;
    colorID[2] = 0;
    SceneObject des(colorID,"ground",desk2);
    des.Picking();
    SceneObjects.append(des);

    glTranslatef(0.0f,alt,12.0f);
    colorID[0] = 2;
    colorID[1] = 0;
    colorID[2] = 0;
    SceneObject aircr(colorID,"aircraft",aircraft2);
    aircr.Picking();
    SceneObjects.append(aircr);
    glTranslatef(0.0f,-alt,0.0f);

    glPushMatrix();
        glColor3f(0, 0, 0);
        DrawCircle(0.0f,0.0f,0.0f,4.0f,12);
    glPopMatrix();

    glTranslatef(0.0f,0.0f,-12.0f);

    for (int i=0; i< Beacons.size(); i++) {
        Beacons[i].picking();
    }
    glTranslatef(0.0,alt,12.0);

    mouseSelectObject(lastPos.x(), lastPos.y());
}

void GLWidget::drawInfo(QString info)
{
    QStringList lines = info.split("\n");
    glDisable(GL_LIGHTING);
    glDisable(GL_DEPTH_TEST);
    qglColor(Qt::white);
    int i = 13;
    foreach(QString line, lines)
    {
        if (!line.isEmpty()) {
            renderText(-15.5f, i*1.1f, 0, line.mid(0,line.indexOf(":")), QFont("Arial", 14, QFont::Bold, false) );
            renderText(-13.0f, i*1.1f, 0, line.mid(line.indexOf(":")+1), QFont("Arial", 14, QFont::Bold, false) );
        }
        i--;
    }

    renderText(5.0,-15.6,0,"map tiles © OpenStreetMap contributors",QFont("Arial",8,QFont::Normal,false));
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHTING);
}

QString GLWidget::getInfo()
{
    QString info;
    QTextStream str(&info);
    str << "IAS:" << QString::number(mpsToMach((airspeed < 0.001) ? 0 : airspeed),'f',3) << "M\n";
    str << "TRK:" << QString::number(heading,'f',1).append("°\n"); //HEA
    str << "ALT:" << (int)toFeet(altitude) << "ft\n";
    //str << "VS :" << vertical_speed << "\n";
    str << "LAT:" << decimalToDegrees(latitude) << "\n";
    str << "LON:" << decimalToDegrees(longitude) << "\n";
    return info;
}

void GLWidget::paintGL()
{
    getGround();    

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glPushMatrix();
    glRotated(xRot / 16.0, 1.0, 0.0, 0.0);
    glRotated(yRot / 16.0, 0.0, 1.0, 0.0);
    glRotated(zRot / 16.0, 0.0, 0.0, 1.0);
    glTranslatef(0.0,-5.0,0.0);

    float alt = 2.0 + altitude/(init->range_alt/14);
    if (!editON || mode == DRAW || mode == BEACON_ALT || mode == BEACON_MAP || mode == BEACON_IAS)
    {
        this->paintNormal(alt);
    }
    else if (mode == SELECT)
    {
        this->paintPicking(alt);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glTranslatef(0.0,-1*alt,-12.0);
        if (mode == GETPIXEL) {
            makeGrad();
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        }

        this->paintNormal(alt);
        if (mode != BEACON_ALT && mode != BEACON_MAP && mode != BEACON_IAS) mode = DRAW;
    }
    else if (mode == GET_BEACON_PIXEL) {
        makeGrad();
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        this->paintNormal(alt);
        //mode = DRAW;
    }
    else {
        this->paintNormal(alt);
    }

    glPopMatrix();

    if (infoON)
        drawInfo(getInfo());

}

void GLWidget::resizeGL(int width, int height)
{
    glViewport(0, 0, width, height);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glFrustum(-2.0, +2.0, -2.0, 2.0, 5.0, 75.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslated(0.0, 0.0, -40.0);
}

void GLWidget::mouseSelectObject(int x, int y)
{
    // render every object in our scene
    // suppose every object is stored in a list container called SceneObjects
    QList<SceneObject>::iterator itr;
    QList<Beacon>::iterator itrb;

    // get color information from frame buffer
    unsigned char pixel[3];
    GLint viewport[4];
    glGetIntegerv(GL_VIEWPORT, viewport);
    glReadPixels(x, viewport[3] - y, 1, 1, GL_RGB, GL_UNSIGNED_BYTE, pixel);

    // now our picked screen pixel color is stored in pixel[3]
    // so we search through our object list looking for the object that was selected
    itr = SceneObjects.begin();
    while(itr != SceneObjects.end())
    {
         if(itr->m_colorID[0] == pixel[0] && itr->m_colorID[1] == pixel[1] && itr->m_colorID[2] == pixel[2])
         {
              // flag object as selected
              SetSelected(&(*itr));
              break;
         }
         itr++;
    }

    if (itr == SceneObjects.end()) {
        itrb = Beacons.begin();
        while(itrb != Beacons.end())
        {
             if(itrb->m_colorID[0] == pixel[0] && itrb->m_colorID[1] == pixel[1] && itrb->m_colorID[2] == pixel[2])
             {
                  // flag object as selected
                  SetSelectedB(&(*itrb),false);
                  break;
             }
             else if (itrb->m_colorID[2] == pixel[0] && itrb->m_colorID[1] == pixel[1] && itrb->m_colorID[2] == pixel[2])
             {
                 SetSelectedB(&(*itrb),true);
                 break;
             }
             else if (itrb->m_colorID[2] == pixel[0] && itrb->m_colorID[1] == pixel[1] && 255 == pixel[2]) {
                 SetSelectedIAS(&(*itrb));
                 break;
             }
             itrb++;
        }
    }
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_TEXTURE_2D);
}

void GLWidget::SetSelected(SceneObject *itr)
{
    if(itr->getName() == "ground") {
        mode = GETPIXEL;
    }
}

void GLWidget::SetSelectedB(Beacon *itrb,bool top)
{
    if (top) mode = BEACON_ALT;
    else mode = BEACON_MAP;
    beacon_index = itrb->index - 1;
}

void GLWidget::SetSelectedIAS(Beacon *itrb)
{
    mode = BEACON_IAS;
    beacon_index = itrb->index - 1;
}

void GLWidget::mousePressEvent(QMouseEvent *event)
{
    lastPos = event->pos();

    if (mode == DRAW) mode = SELECT;
}

void GLWidget::mouseMoveEvent(QMouseEvent *event)
{
    int dx = event->x() - lastPos.x();
    int dy = event->y() - lastPos.y();

    if (event->buttons() & Qt::LeftButton) {
        if (mode == BEACON_ALT) {
            Beacons[beacon_index].changeAlt(dy);
        }
        else if (mode == BEACON_MAP) {
            Beacons[beacon_index].setNewPos(dx,dy);
            mode = GET_BEACON_PIXEL;
        }
        else if (mode == BEACON_IAS) {
            float prev_xx = 128.0f;
            if (beacon_index > 0)
                prev_xx = Beacons[beacon_index-1].Xx();
            Beacons[beacon_index].changeIAS(dx,dy,prev_xx);
        }
        else if (mode != GET_BEACON_PIXEL){
            //setXRotation(xRot + 8 * dy);
            setYRotation(yRot + 8 * dx);
        }
    } else if (event->buttons() & Qt::RightButton) {
        setXRotation(xRot + 8 * dy);
        //setZRotation(zRot + 8 * dx);
    }
    lastPos = event->pos();
}

void GLWidget::mouseReleaseEvent(QMouseEvent *event)
{
    Q_UNUSED ( event);
    mode = DRAW;
}

void GLWidget::advanceGears()
{
    updateGL();
}

void GLWidget::onEdit()
{
    editON = (editON) ? false : true;
}

GLuint GLWidget::makeCraft(objloader plane, const GLfloat *reflectance, const GLfloat ratio, int mode=DRAW)
{
    //http://tf3dm.com/3d-model/boeing-737-25104.html
    QString file_name = QString(init->aircraft_model);
    QByteArray arr = file_name.toLocal8Bit();
    char *str = arr.data();

    int i = plane.load(str);

    if (!i)
        exit(0);

    vector<Vector> ver;
    vector<Vector> nor;
    vector<Face> fac;

    plane.getFaces(fac,ver,nor);

    GLuint list = glGenLists(1);    
    glNewList(list, GL_COMPILE);
    if (mode == DRAW)
    {
        glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, reflectance);
        glShadeModel(GL_SMOOTH);
    }
    glScalef(ratio,ratio,ratio);
    if (mode==DRAW)
        plane.draw();
    else
        plane.drawVertices();

    glEndList();

    return list;
}

void GLWidget::drawCraft(GLdouble dx, GLdouble dy, GLdouble dz, GLuint air)
{
    glPushMatrix();
    glTranslated(dx, dy, dz);
    //glRotated(90.0, 0.0, 0.0, 1.0);
    glCallList(air);
    glPopMatrix();
}

void GLWidget::normalizeAngle(int *angle)
{
    while (*angle < 0)
        *angle += 360 * 16;
    while (*angle > 360 * 16)
        *angle -= 360 * 16;
}

void GLWidget::rotatedPos(float &x, float &y, int px, int py)
{
    rotated(x, y, px, py, heading);
}

void GLWidget::unrotatedPos(int &x, int &y, int px, int py)
{
    float xx = (float) x;
    float yy = (float) y;
    rotated(xx, yy, px, py, -1*heading);
    x = (int) xx;
    y = (int) yy;
}

void GLWidget::getGround()
{
    int x, y;
    geoToTile(x, y, latitude,longitude);

    float lat_first, lon_first, lat_second, lon_second;
    geoPerPix(x, y, px_x, px_y, lat_first, lon_first, lat_second, lon_second);

    int dx = (TILE_WIDTH / 2) - ((longitude - lon_first) / px_x);
    int dy = (TILE_HEIGHT / 2) - ((latitude - lat_first) / px_y);

    QPixmap result(5*TILE_WIDTH, 5*TILE_HEIGHT);//,QImage::Format_ARGB32);
    QPixmap res(BUF_WIDTH, BUF_HEIGHT);

    //TODO vykreslovat okno v bufferu tvorenym aktualni dlazdici a 8-okolim !!!
    // - aktualni dlazdice slozena ze 4 s vetsim zoomem (detailnejsi zoom=9)
    QPainter painter;
    painter.begin(&result);
    painter.translate(TILE_WIDTH + BUF_WIDTH/2 - dx,TILE_HEIGHT + BUF_HEIGHT/2 - dy);
    painter.rotate(-1*heading);
    painter.translate(-TILE_WIDTH - BUF_WIDTH/2 + dx,-TILE_HEIGHT -BUF_HEIGHT/2 + dy);
    painter.drawPixmap(5*TILE_WIDTH, 5*TILE_HEIGHT,result);

    QPixmap tile;
    int size_x, size_y;
    size_y = 0;
    for (int yy = y - 2; yy <= y + 2; yy++) {
      size_x = 0;
      for (int xx = x - 2; xx <= x + 2; xx++) {
        QString file_name = "./OSM/" + QString::number(zoom) + "/" + QString::number(xx) + "/" + QString::number(yy) + ".png";
        tile.load(file_name);
        painter.drawPixmap(size_x,size_y,tile);
        size_x += TILE_WIDTH;
      }
      size_y += TILE_HEIGHT;
    }
    res = result.copy(TILE_WIDTH,TILE_HEIGHT,BUF_WIDTH,BUF_HEIGHT);
    //result.save("result_or.png","PNG",10);
    result.swap(res);
    painter.end();

    //result.save("result.png","PNG",10);
    painter.begin(&result);
    tile_buf = result.toImage().copy(TILE_WIDTH - dx, TILE_HEIGHT/2 - dy, TILE_WIDTH, TILE_HEIGHT);
    painter.fillRect(0,0,BUF_WIDTH, BUF_HEIGHT,QBrush(QColor(10,10,40,100)));
    painter.end();
    n_tiles = result.toImage().copy(TILE_WIDTH/2 - dx, -1 * dy, 2*TILE_WIDTH, 2*TILE_HEIGHT);

}

void GLWidget::makeGround(GLuint plane) {
    glPushMatrix();
    static const GLfloat reflectance[4] = { 10.0f, 10.0f, 10.0f, 10.0f };
    if (plane == desk) {
        LoadTexture(TILE_WIDTH,TILE_HEIGHT);//256, 256);
        glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE,reflectance);
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, g_texture);
    }
    else if (plane == neighbours) {
        LoadNeighbours(2*TILE_WIDTH,2*TILE_HEIGHT);
        glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE,reflectance);
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, n_texture);
    }
    glCallList(plane);
    glPopMatrix();
}

void GLWidget::makeGrad()
{
    glPushMatrix();
    unsigned char pixel[3];
    static const GLfloat reflectance[4] = { 10.0f, 10.0f, 10.0f, 10.0f };
    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE,reflectance);
    glEnable(GL_TEXTURE_2D);

    int width = TILE_WIDTH;
    int height = TILE_HEIGHT;
    this->LoatTxtr(width, height, gradient, g_texture);

    glCallList(desk);
    glPopMatrix();
    GLint viewport[4];
    glGetIntegerv(GL_VIEWPORT, viewport);
    glReadPixels(lastPos.x(), viewport[3] - lastPos.y(), 1, 1, GL_RGB, GL_UNSIGNED_BYTE, pixel);

    float x,y,non_rotated_x, non_rotated_y;
    rotatedPos(non_rotated_x, non_rotated_y, (int)pixel[1],(int)pixel[2]);
    pixToGeo((int)non_rotated_x, (int)non_rotated_y, x,y);

    //if EDIT
    if (mode != GET_BEACON_PIXEL) {        
        ParsedData data;        
        if (!Beacons.isEmpty()) { //follow last beacon data
            data.alt = Beacons[beacon_index].Altitude();
            data.ias = Beacons[beacon_index].Airspeed();
        }
        else {
            data.alt = altitude;
            data.ias = mpsToMach(airspeed);
        }
        data.lon = x;
        data.lat = y;
        data.ias = (data.ias > init->min_speed) ? data.ias : init->min_speed;
        QString name = "beacon_";
        Beacons.append(Beacon(name, data, Beacons.size(), init));
    }
    else if (pixel[1] != 0 && pixel[2] != 0) { //&& mode == GET_BEACON_PIXEL
        Beacons[beacon_index].changeMap(x,y);
    }
}

GLuint GLWidget::createDesk(const GLfloat ratio)
{
    GLfloat neg = -1*ratio;
    GLuint list = glGenLists(2);
    glNewList(list, GL_COMPILE);
    glBegin(GL_QUADS);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(neg, 0.0f, neg);
        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(neg, 0.0f, ratio);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f( ratio, 0.0f, ratio);
        glTexCoord2f(1.0f, 0.0f);
        glVertex3f( ratio, 0.0f, neg);
    glEnd();
    glEndList();
    return list;
}

void GLWidget::LoatTxtr(int &width, int &height, QImage &buf, GLuint &texture)
{
    if (width <= 0)
        width = buf.width();
    if (height <= 0)
        height = buf.height();
    if (width != buf.width() || height != buf.height())
        buf = buf.scaled(width, height, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);

    glBindTexture(GL_TEXTURE_2D, texture);

    // Works on x86, so probably works on all little-endian systems.
    // Does it work on big-endian systems?
    glTexImage2D(GL_TEXTURE_2D, 0, 4, buf.width(), buf.height(), 0,
        GL_BGRA, GL_UNSIGNED_BYTE, buf.bits());


    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
}

void GLWidget::LoadTexture(int width, int height)
{

    this->LoatTxtr(width, height, tile_buf, g_texture);
    //comment these two if performance is too slow
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);
}

void GLWidget::LoadNeighbours(int width, int height)
{
    this->LoatTxtr(width, height, n_tiles, n_texture);
}

inline float GLWidget::rad2deg(float r) {
    return 180.0f * r / M_PI;
}

inline float GLWidget::deg2rad(float d) {
    return d * M_PI / 180.0f;
}

inline void GLWidget::posToGeo(const int x, const int y, float& lat, float& lon) {
    int abs_x = x-pos_x;
    int abs_y = y-pos_y;

    lat = (((tiles * TILE_HEIGHT / 2) - abs_y) * 2 * M_PI ) / (tiles*TILE_HEIGHT);
    lat = rad2deg(asin(tanh(lat)));
    lon = ((abs_x - (tiles* TILE_WIDTH /2)) * 2 * M_PI) / ( tiles * TILE_WIDTH);
    lon = rad2deg(lon);

    //  printf("posToGeo: %d %d %f %f\n", abs_x, abs_y,lat, lon);

}
inline void GLWidget::geoToPos(int& x, int& y, const float lat, const float lon) {
    float latR = atanh(sin(deg2rad(lat)));
    float lonR = deg2rad(lon);

    x =  lonR * tiles * TILE_HEIGHT / (2 * M_PI) + tiles * TILE_HEIGHT / 2;
    y = -latR * tiles * TILE_WIDTH  / (2 * M_PI) + tiles * TILE_WIDTH  / 2;
    //  printf("geoToPos: %f %f %d %d\n", lat, lon, x, y);
}

inline void GLWidget::geoToTile(int& xtile, int& ytile, const float lat, const float lon) {
    float latR = lat * M_PI/180.0;
    float n = pow(2.0,zoom);

    //http://wiki.openstreetmap.org/wiki/Slippy_map_tilenames
    xtile = (int)(floor((lon + 180.0) / 360.0 * n));
    ytile = (int)(floor((1.0 - log( tan(latR) + 1.0 / cos(latR)) / M_PI) / 2.0 * n));
}

inline void GLWidget::tileToGeo(int xtile, int ytile, float& lat, float& lon) {
    double n = M_PI - 2.0 * M_PI * ytile / pow(2.0, zoom);
    lat = 180.0 / M_PI * atan(0.5 * (exp(n) - exp(-n)));
    lon = xtile / pow(2.0, zoom) * 360.0 - 180;
}

inline void GLWidget::geoPerPix(int xtile, int ytile, float& x, float& y, float& lat_first, float &lon_first, float& lat_second, float& lon_second) {
    int xxtile, yytile;

    if (xtile < xtile_max)
       xxtile = xtile + 1;
    else {
       xxtile = xtile;
       xtile = xtile - 1;
    }
    if (ytile < ytile_max)
       yytile = ytile + 1;
    else {
       yytile = ytile;
       ytile = ytile - 1;
    }

    tileToGeo(xtile, ytile, lat_first, lon_first);
    tileToGeo(xxtile, yytile, lat_second, lon_second);

    x = (lon_second - lon_first) / TILE_WIDTH;
    y = (lat_second - lat_first) / TILE_HEIGHT;
    //qDebug() << lat_first << lon_first << lat_second << lon_second << x << y;
}

void GLWidget::setMapCenter(const float lat, const float lon) {

    int x, y;
    geoToPos(x, y, lat, lon);
    QSize s = size();

    pos_x = s.width() / 2 - x;
    pos_y = s.height() / 2 - y;

    //  printf("setMapCenter: %d %d %d %d %f %f\n", x, y, pos_x, pos_y, lat, lon);
    //update();

}

inline void GLWidget::pixToGeo(int px, int py, float &x, float &y) {
    x = longitude + (px - 127) * px_x;
    y = latitude  + (py - 255) * px_y;
}

inline void GLWidget::geoToPix(int &px, int &py, float x, float y) {
    px = (x - longitude)/px_x + 127;
    py = (y - latitude )/px_y + 255;
}

void GLWidget::onPacketReceived(QString packet) {
    //pkt = packet;
    this->parseData(packet);
}

void GLWidget::parseData(QString input) {
    //qDebug() << input;
    //ParsedData ret;
    int i = input.indexOf(":ALT:");
    QString str = input.mid(i+5,input.indexOf(":",i+5)-i-5);
    altitude = str.toFloat();

    i = input.indexOf(":LAT:");
    str = input.mid(i+5,input.indexOf(":",i+5)-i-5);
    //ret.lat = str.toFloat();
    latitude = str.toFloat();

    i = input.indexOf(":LON:");
    str = input.mid(i+5,input.indexOf(":",i+5)-i-5);
    longitude = str.toFloat(); //ret.lon = str.toFloat();

    i = input.indexOf(":HEA:");
    str = input.mid(i+5,input.indexOf(":",i+5)-i-5);
    heading = str.toFloat(); //ret.hea = str.toFloat();

    i = input.indexOf(":IAS:");
    str = input.mid(i+5,input.indexOf(":",i+5)-i-5);
    airspeed = str.toFloat();

    i = input.indexOf(":VS_:");
    str = input.mid(i+5,input.indexOf(":",i+5)-i-5);
    vertical_speed = str.toFloat();

    //qDebug() << toFeet(altitude) << latitude << longitude << heading;
    //return ret;
}

QString GLWidget::setNewCourse(float b_lat,float b_lon,float b_alt, float b_ias) {
    QString pkto;
    QTextStream str(&pkto);
    //SET:APIAS:hodnota:APALT:hodnota:APTT_:hodnota:END;

    float a = b_lon - this->longitude;
    float b = b_lat - this->latitude;
    float c = sqrt(a*a + b*b);
    float sing = (a>0.0f) ? -1.0f : 1.0f;
    float angle = sing * (rad2deg(asin(b/c)) - 90.0f);
    //qDebug() << a << b << c << angle << asin(b/c);

    str << "SET";
    str << ":APIAS:" << machToMPS(b_ias);
    str << ":APALT:" << b_alt;
    str << ":APTT_:" << angle;
    str << ":END\n";
    return pkto;
}

void GLWidget::disableEdit()
{
    editON = false;
}
