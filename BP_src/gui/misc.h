#ifndef MISC_H
#define MISC_H

#define TILE_WIDTH  256
#define TILE_HEIGHT 256

#define BUF_WIDTH  768
#define BUF_HEIGHT 768

#define DRAW     20
#define SELECT   21
#define GETPIXEL 22
#define BEACON_ALT 23
#define BEACON_MAP 24
#define GET_BEACON_PIXEL 25
#define BEACON_IAS 26
#define GET_BEACON_PIXEL_IAS 27

#define MAX_ALT   11278 //37000 ft
#define MIN_ALT     600 //1968.5 ft
#define RANGE_ALT 10678

#define LAT_KM 111 //approximate, for reference only
#define DISTANCE_MIN 0.025 // lat/lon degrees
#define MAX_SPEED 1.0f //Mach
#define MIN_SPEED 0.2f

#include <QGLWidget>

typedef struct
{
    float lon;
    float lat;
    float hea;
    float alt; //metry
    float ias; //m/s
    float vs;  //m/s
} ParsedData;

QImage createGradient(int width, int height);
void intToPixel(int val, int &r, int &g, int &b);

float toMeters(float feet);
float toFeet(float meters);

void DrawCircle(float cx, float cy, float z, float r, int num_segments);
void DrawCircleEmpty(float cx, float cy, float z, float r, int num_segments);

void DrawLine(float x, float y, float z, float xx, float yy, float zz, const GLfloat *reflectance);
void DrawArrow (float x1, float alt, float y1, float x2, float y2, float len);

float mpsToMach(float mps);
float machToMPS(float M);

QString decimalToDegrees(float dec);

void rotated(float &x, float &y, int px, int py, float angle);

#endif // MISC_H
