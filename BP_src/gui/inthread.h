#ifndef INTHREAD_H
#define INTHREAD_H

#include <QThread>
#include <QTcpSocket>
#include <QMutex>

#define TCP_IP "147.229.13.183"
#define TCP_PORT 12321 //12323

class inThread : public QThread
{
    Q_OBJECT
public:
    int endflag;
    explicit inThread(QObject *parent = 0);

    QString getData();

    void init(QString ini_tcp_ip, unsigned ini_tcp_port);

    void run();

signals:
    void packetReceived(QString);    

public slots:
    void onPacketSending(QString packet);

private:
    QString pkt;
    QString pktOut;
    QString tcp_ip;
    unsigned tcp_port;
};

#endif // INTHREAD_H
