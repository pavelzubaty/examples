#ifndef BEACON_H
#define BEACON_H

#include <objloader.h>
#include <sceneobject.h>
#include <misc.h>
#include <QGLWidget>
#include <initdata.h>

class Beacon
{
public:
    Beacon(QString n, ParsedData data, int in, InitData *ini);
    void draw(int x, int y, int rot);
    void picking();
    void changeAlt(int dy);
    void changeMap(float x, float y);
    void setNewPos(int dx, int dy);
    void changeIAS(int dx, int dy, float prev_xx);
    void drawLine(float pre_x, float pre_alt, float pre_y, const GLfloat *reflectance);
    void drawConnection(const GLfloat *reflectance, const GLfloat *reflectance2);

    float Latitude() const { return latitude; }
    float Longitude() const { return longitude; }
    float Altitude() const { return altitude; }
    float Airspeed() const { return airspeed; }
    QString Name() const { return name; }
    float Alt() const { return alt; }
    float Px() const { return px; }
    float Py() const { return py; }
    float Xx() const { return xx; }
    //float Index() const { return index; }

    unsigned char m_colorID[4];
    int px_dx, px_dy;
    int index;
private:
    void createMapMark();
    float latitude,longitude,altitude, airspeed; //TODO IAS, VS?, HEA?
    QString name;
    //TODO ? Beacon *next = NULL;
    objloader vertical_mark;
    //map_mark
    GLuint v_mark;
    GLuint m_mark;    
    float alt, px, py, prev_x, prev_alt, prev_y, xx;
    int yRot;
    InitData *init;
    //int pos_x, pos_y, prev_pos_x, prev_pos_y;
};

#endif // BEACON_H
