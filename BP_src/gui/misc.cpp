#include "misc.h"
#include <math.h>
#include <qdebug.h>
#include <qstring.h>
#include <QTextStream>

float toMeters(float feet) {
    return feet / 3.2808;
}

float toFeet(float meters) {
    return meters * 3.2808;
}

QImage createGradient(int width, int height)
{
    QImage img = QImage(width, height, QImage::Format_ARGB32);
    QColor c;
    for (int i=0;i<height;i++) { //TODO step_x, step_y, memcpy namisto setPixel
        for (int j=0;j<width;j++) {
            c.setRgb(1,i,j);
            //rawData[i][j] = c.rgba();
            img.setPixel(i,j,c.rgba());
        }
    }
    img.save("gradient.png","PNG",100);
    return img;
}

void intToPixel(int val, int &r, int &g, int &b)
{
    r = 0;
    g = val/255;
    b = val%255;
}

//http://slabode.exofire.net/circle_draw.shtml
void DrawCircle(float cx, float cy, float z, float r, int num_segments)
{
    float theta = 2 * 3.1415926 / float(num_segments);
    float c = cosf(theta);//precalculate the sine and cosine
    float s = sinf(theta);
    float t;

    float x = r;//we start at angle = 0
    float y = 0;

    glBegin(GL_TRIANGLE_FAN);
    glTranslatef(0.01, 0.01, 0.01);
    //glBegin(GL_LINE_LOOP);
    for(int ii = 0; ii < num_segments; ii++)
    {
        glNormal3f(0.0f,1.0f,0.0f);
        glVertex3f(x + cx, z, y + cy);//output vertex

        //apply the rotation matrix
        t = x;
        x = c * x - s * y;
        y = s * t + c * y;
    }
    glEnd();
}

void DrawCircleEmpty(float cx, float cy, float z, float r, int num_segments)
{
    float theta = 2 * 3.1415926 / float(num_segments);
    float c = cosf(theta);//precalculate the sine and cosine
    float s = sinf(theta);
    float t;

    float x = r;//we start at angle = 0
    float y = 0;

    glBegin(GL_LINE_LOOP);
    for(int ii = 0; ii < num_segments; ii++)
    {
        glNormal3f(0.0f,1.0f,0.0f);
        glVertex3f(x + cx, z, y + cy);//output vertex

        //apply the rotation matrix
        t = x;
        x = c * x - s * y;
        y = s * t + c * y;
    }
    glEnd();
}

void DrawLine (float x, float y, float z, float xx, float yy, float zz, const GLfloat *reflectance) {
    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, reflectance);
    glLineWidth( 2.0f ); //1.5f, 1.1,0.1
    glBegin(GL_LINES);
        glVertex3f(x, y, z);
        glVertex3f(xx, yy, zz);
    glEnd();
}

void DrawArrow (float x1, float alt, float y1, float x2, float y2, float len) {
    const float siz = 2.0f;
    float dx = x2 - x1;
    float dy = y2 - y1;
    float dd = (fabs(dx) < 2.0f && fabs(dy) < 2.0f) ? 4.0f : 1.0f;

    float sizx = (x2 - x1)*0.3f*dd;
    float sizy = (y2 - y1)*0.3f*dd;

    dx *= len*dd;
    dy *= len*dd;

    glBegin(GL_TRIANGLES);
        glNormal3f(x1+dx,alt-siz,y1+dy);
        glVertex3f(x1+dx,alt-siz,y1+dy);
        glNormal3f(x1+dx,alt+siz,y1+dy);
        glVertex3f(x1+dx,alt+siz,y1+dy);
        glNormal3f(x1+dx+sizx,alt,y1+dy+sizy);
        glVertex3f(x1+dx+sizx,alt,y1+dy+sizy);

        glNormal3f(x1+sizx,alt-siz/2,y1+sizy);
        glVertex3f(x1+sizx,alt-siz/2,y1+sizy);
        glNormal3f(x1+sizx,alt+siz/2,y1+sizy);
        glVertex3f(x1+sizx,alt+siz/2,y1+sizy);
        glNormal3f(x1+dx,alt-siz/2,y1+dy);
        glVertex3f(x1+dx,alt-siz/2,y1+dy);

        glNormal3f(x1+sizx,alt+siz/2,y1+sizy);
        glVertex3f(x1+sizx,alt+siz/2,y1+sizy);
        glNormal3f(x1+dx,alt-siz/2,y1+dy);
        glVertex3f(x1+dx,alt-siz/2,y1+dy);
        glNormal3f(x1+dx,alt+siz/2,y1+dy);
        glVertex3f(x1+dx,alt+siz/2,y1+dy);
    glEnd();
}

float mpsToMach(float mps) {
    return mps/340.0f;
}

float machToMPS(float M) {
    return M*340.0f;
}

QString decimalToDegrees(float dec) {
    int degrees = ((int) dec);
    int minutes = (int) ((dec-degrees) * 60);
    float seconds = (dec-degrees) * 60;
    seconds = (seconds - (int) seconds) * 60;

    QString ret;
    QTextStream str(&ret);
    str.setFieldWidth(3);
    str.setFieldAlignment(QTextStream::AlignRight);
    str.setPadChar('0');
    str << QString::number(degrees).append("°");
    str << QString::number(minutes).append("'");
    str << QString::number((int)seconds).append('"');
    str.setFieldAlignment(QTextStream::AlignLeft);
    return ret;
}

inline float deg2rad(float d) {
    return d * M_PI / 180.0f;
}
void rotated(float &x, float &y, int px, int py, float angle) {
    x = (px - TILE_WIDTH/2)*cos(deg2rad(angle))
        + TILE_WIDTH/2 - (py - TILE_HEIGHT)*sin(deg2rad(angle));
    y = (py - TILE_HEIGHT)*cos(deg2rad(angle))
        + TILE_HEIGHT + (px - TILE_WIDTH/2)*sin(deg2rad(angle));
}


