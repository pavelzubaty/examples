#ifndef INITDATA_H
#define INITDATA_H

#include <iostream>
#include <fstream>
#include <QStringList>

class InitData
{
public:
    explicit InitData();

    QString tcp_ip;
    int tcp_port;

    int max_alt;
    int min_alt;
    int range_alt;

    float max_speed;
    float min_speed;

    QString aircraft_model;
    float aircraft_model_ratio;
    QString beacon_model;
    float beacon_model_ratio;
    int zoom;
private:
    void parseLine(char *line);
};

#endif // INITDATA_H
