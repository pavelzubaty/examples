#include "sceneobject.h"

BaseObject::BaseObject()
      {
           m_colorID[0] = gColorID[0];
           m_colorID[1] = gColorID[1];
           m_colorID[2] = gColorID[2];

           gColorID[0]++;
           if(gColorID[0] >= 255)
           {
                gColorID[0] = 0;
                gColorID[1]++;
                if(gColorID[1] >= 255)
                {
                     gColorID[1] = 0;
                     gColorID[2]++;
                }
           }
      }

BaseObject::~BaseObject()
{

}

unsigned char BaseObject::gColorID[3] = {0, 0, 0};

SceneObject::SceneObject(unsigned char colorID[4],QString name, GLuint vertices)
{
    m_colorID[0] = colorID[0];
    m_colorID[1] = colorID[1];
    m_colorID[2] = colorID[2];

    m_name = name;
    obj = vertices;
}

SceneObject::~SceneObject()
{

}
/*
void SceneObject::Init()
{

}*/

void SceneObject::Picking()
{

    // set mesh position
    glPushMatrix();
    //glTranslatef(m_position[0], m_position[1], m_position[2]);

    glColor3f(m_colorID[0]/255.0f, m_colorID[1]/255.0f, m_colorID[2]/255.0f);

    // render object's vertices here
    glCallList(obj);
    glPopMatrix();
}
