#include "inthread.h"

inThread::inThread(QObject *parent) :
    QThread(parent)
{
    this->endflag = 0;
    this->tcp_ip = TCP_IP;
    this->tcp_port = TCP_PORT;
}

void inThread::init(QString ini_tcp_ip, unsigned ini_tcp_port)
{
    this->tcp_ip = ini_tcp_ip;
    this->tcp_port = ini_tcp_port;
}

void inThread::run()
{
    //qDebug() << "thread starts";
    while(endflag==0) {
        QMutex mutex;
        mutex.lock();
        pkt = this->getData();
        mutex.unlock();

        if (!pkt.isEmpty())
            emit packetReceived(pkt);
        this->msleep(50);
    }
}

void inThread::onPacketSending(QString packet) {
    pktOut = packet;
}

QString inThread::getData(){
    QTcpSocket mySocket;
    QString recData;

    //qDebug() << tcp_ip << tcp_port;
    //connect
    mySocket.connectToHost(tcp_ip, tcp_port);
    if(mySocket.waitForConnected(3000)){
        // send data
        mySocket.write("GETDATA\n");
        mySocket.flush();
        mySocket.waitForBytesWritten(3000);
        mySocket.waitForReadyRead(3000);
        // get data
        recData = mySocket.readAll().constBegin();
        //mySocket.close();

    }else
    {
        qDebug() << ("Not Connected!");
    }

    mySocket.disconnectFromHost();
    mySocket.close();
    this->msleep(50);
    // SET
    mySocket.connectToHost(tcp_ip, tcp_port);
    if(mySocket.waitForConnected(3000)){
        // send data
        //set:APIAS:hodno:APALT:HODNO:APTT_:hodnot:END;
        mySocket.write(pktOut.toStdString().c_str());
        mySocket.flush();
        mySocket.waitForBytesWritten(3000);
        mySocket.waitForReadyRead(3000);
        // get data
        //qDebug() << mySocket.readAll().constBegin();
        //mySocket.close();
        mySocket.readAll().constBegin();

    }else
    {
        qDebug() << ("Not Connected!");
    }
    mySocket.disconnectFromHost();
    mySocket.close();

    return recData;

}
