# zip of two numbers

def lenn(num):
    i = 1
    while (num>9):
        i += 1
        num = num // 10
    return i

def numToList(num):
    L = []
    while (num>9):
        L.append(num % 10)
        num = num // 10
    L.append(num)
    
    return L
    
def solution(A, B):
    # write your code in Python 2.7
    if (A < 0 or B < 0):
        return -1
        
    if (lenn(A) + lenn(B) > 8):
        return -1
        
    C = 0
    listA = numToList(A)
    listB = numToList(B)
    lenA = len(listA)
    lenB = len(listB)
    lenC = lenA + lenB
    i = 0
    
    while (i < lenC):
        if (len(listA)>0):
            C = 10*C + listA.pop()
        if (len(listB)>0):
            C = 10*C + listB.pop()
        i += 1
    return C
    
