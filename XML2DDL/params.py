#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#XTD:xzubat02

##############################################################################
#  XTD: XML2DDL (XML to DDL)                                                 #
#  projekt predmetu IPP 2013/2014                                            #
#                                                                            #
#  ZPRACOVANI PARAMETRU                                                      #
#   - class Params                                                           #
#                                                                            #
#  Tento modul ridi zpracovani parametru, se kterymi je spousten hlavni mo-  #
#  dul (skript xtd.py). Ty jsou nasledne vyuzivany v hlavnim modulu v ramci  #
#  instance tridy Submerge.                                                  #
#                                                                            #
#  VYPRACOVAL:                                                               #
#   Pavel Zubaty, 13.4.2014                                                  #
#                                                                            #
##############################################################################

import sys

class Params:
    Help = """***XTD: XML2DDL***\n\
Program napsal Pavel Zubaty jako skolni projekt predmetu Principy programovacich
jazyku (v programovacim jazyce Python), posledni upravy: 13.4.2014

Tato utilita zpracovava vstupni XML soubor (viz nize) do vychoziho souboru, kte-
ry odpovida serii dotazu CREATE dotazovaciho jazyka SQL. Jinymi slovy: prevede 
tabulku XML na tabulky SQL.

    Argumenty programu:
        --input=[input_file] - nacita z XML souboru [input_file] misto standard-
                               niho vstupu STDIN
        --output=[output_file] - vystup je ukladan do souboru [output_file] ve 
                                 formatu SQL namisto standardniho vystupu STDOUT
        --header='nejaky text' - pridava na zacatek souboru hlavicku omezenou u-
                                 vozovkami ci apostrofy
        --etc=[X] - pro cislo [X]>=0 urcuje maximalni pocet sloupcu vzniklych ze
                   stejnojmenych podelementu
        -a - nebudou se generovat sloupce z atributu ve vstupnim XML souboru
        -b - (nesmi byt soucasne s --etc=[X]) pokud bude element obsahovat vice
             podelementu stejneho nazvu, bude se uvazovat, jako by zde byl pouze
             jediny takovy
        -g - vystupem bude XML soubor zachycujici relace. Lze kombinovat se vsemi 
             parametry mimo --help"""
             
    def __init__(self):
        self.input_flag = False  #bude se nacitat ze souboru?
        self.input_file = 0      #stdin
        self.output_flag = False #bude se vypisovat do souboru?
        self.output_file = 1     #stdout
        self.header_flag = False #bude vysledny text obsahovat hlavicku?
        self.header = 0          #retezec je prazdny
        self.etc_flag = False    #bude existovat omezeni na pocet sloupcu?
        self.etc = -1
        self.a_flag = False      #nebudou se generovat sloupce z atributu vstup. XML?
        self.b_flag = False      #nekombinovat s etc, pro vice subelementu stejneho jmena se vytvori jeden sloupec
        
        self.g_flag = False
        
    def GetParams(self):
        """
        Trida Params slouzi ke zpracovani parametru.K tomu vyuziva knihovny sys,
        konkretne metodu sys.argv. Pomoci te pak zkontroluje pocet argumentu, 
        jinak postupuje podle zadanych parametru a kontroluje nepovolene kombi-
        nace, nezname parametry ci duplicitni parametry - v takovych pripadech
        konci program s chybovym hlasenim a hodnotou 1.
        
        --input=[input_file] - nacita z XML souboru [input_file] misto standard-
                               niho vstupu STDIN
        --output=[output_file] - vystup je ukladan do souboru [output_file] ve 
                                 formatu SQL namisto standardniho vystupu STDOUT
        --header='nejaky text' - pridava na zacatek souboru hlavicku omezenou u-
                                 vozovkami ci apostrofy
        --etc=[X] - pro cislo [X]>=0 urcuje maximalni pocet sloupcu vzniklych ze
                   stejnojmenych podelementu
        -a - nebudou se generovat sloupce z atributu ve vstupnim XML souboru
        -b - (nesmi byt soucasne s --etc=[X]) pokud bude element obsahovat vice
             podelementu stejneho nazvu, bude se uvazovat, jako by zde byl pouze
             jediny takovy
        -g - vystupem bude XML soubor zachycujici relace. Lze kombinovat se vsemi 
             parametry mimo --help
        
        arguments: initialized class attributes
        return: modifies class attributes
        """
        argc = len(sys.argv)
        if argc == 2: #jeden argument
            args = sys.argv[1]
            if (args[0:6] == "--help"):
                print(self.Help)
                sys.exit(0)
        if argc > 8: #nastane v pripade nepovolennych kombinaci, ktere nemusi vysetrovat
            sys.exit("Vyskytl se duplicitni/neznamy parametr nebo nepovolena kombinace!")
        
        dupla = "Duplicitni parametr "
        for i in range(1, argc): #nemusi dokola kontrolovat pocet argv,
            args = sys.argv[i] #namisto toho je postupne nacita do atributu tridy Params
            if (args[0:6] == "--help"):
                sys.exit("Parametr --help nelze kombinovat s jinym!")
        
            if (args[0:8] == "--input="): #nacita --input=[input_file]
                if self.input_flag: #jednotlive priznaky flag odpovidaji vyskytu daneho parametru
                    sys.exit(dupla + "--input!")
                else:
                    self.input_flag = True
                    self.input_file = args[8:len(args)]
            elif (args[0:9] == "--output="): #nacita --output=[output_file] 
                if self.output_flag:
                    sys.exit(dupla + "--output!")
                else:
                    self.output_flag = True
                    self.output_file = args[9:len(args)]
            elif (args[0:9] == "--header="): #nacita --header='nejaky text'
                if self.header_flag:
                    sys.exit(dupla + "--header!")
                else:
                    self.header_flag = True
                    self.header = args[9:(len(args))]
            elif (args[0:6] == "--etc="): #nacita --etc=[X]
                if self.etc_flag:
                    sys.exit(dupla + "--etc!")
                elif self.b_flag:
                    sys.exit("Nepovolena kombinace -b a -etc!")
                else:
                    self.etc_flag = True
                    try:
                        self.etc = int(args[6:len(args)])
                    except ValueError:
                        sys.exit("Nebyl mozny prevod hodnoty parametru --etc!")
                    if self.etc < 0:
                        sys.exit("Hodnota --etc musi byt nezaporna!")
            elif (args == "-a"): #nacita -a
                if self.a_flag:
                    sys.exit(dupla + "-a!")
                else:
                    self.a_flag = True
            elif (args == "-b"): #nacita -b
                if self.b_flag:
                    sys.exit(dupla + "-b!")
                elif self.etc_flag:
                    sys.exit("Nepovolena kombinace -b a -etc!")
                else:
                    self.b_flag = True
            elif (args == "-g"):
                if self.g_flag:
                    sys.exit(dupla + "-g!")
                else:
                    self.g_flag = True                
            else: #nacita WTF nedefinovany parametr
                sys.exit("Neznamy parametr " + args)
