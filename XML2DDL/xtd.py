#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#XTD:xzubat02

##############################################################################
#  XTD: XML2DDL (XML to DDL)                                                 #
#  projekt predmetu IPP 2013/2014                                            #
#                                                                            #
#  HLAVNI MODUL                                                              #
#   - class Submerge                                                         #
#   - hlavni telo skriptu                                                    #
#                                                                            #
#  Tento modul obsahuje hlavni cast implementace skriptu. Patri sem nacteni  #
#  vstupniho souboru do datoveho XML stromu, jeho zpracovani pomoci instance #
#  tridy Submerge (viz nize) a tisk vystupu do vystupniho souboru.           #
#                                                                            #
#  VYPRACOVAL:                                                               #
#   Pavel Zubaty, 13.4.2014                                                  #
#                                                                            #
##############################################################################

import copy
import re
import xml.etree.ElementTree as etree
from params import *

def printError(msg, code):
    """
    Vypise chybove hlaseni msg na STDERR a ukonci skript s hodnotou code.
    """
    sys.stderr.write(msg)
    sys.exit(code)
                
class Submerge(object):
    """
    Trida Submerge obsahuje stezejni cast programu. Pri sve inicializaci
    nacita zpracovane parametry programu (instance tridy Params) a koren
    datoveho stromu ziskany ze vstupniho XML souboru (k tomu slouzi
    metoda parse tridy xml.etree.ElementTree). Nejdulezitejsi cast
    tvori metoda submerge, ktera vola dilci metody a vraci vystup formou
    textoveho retezce. Ten lze nasledne vlozit do textoveho souboru jako
    SQL dotaz nebo XML soubor relaci v pripade prepinace -g.
    """
    
    def __init__(self, rot, par):
        """
        konstruktor tridy Submerge
        
        @param rot - koren XML datoveho stromu
        @param par - instance tridy Param, parametry programu
        """
        
        self.root = rot
        self.vysledek = ""
        self.relations = {}
        self.p = par #Params
        
        self.children = {} #slovnik, kvuli datovym typum
        self.values = {} #slovnik pro value
        self.attributes = {} #atributy polozky
        self.tables = {} #slovnik, obsahujici tabulky:[polozky neboli sloupce]
    
    def getRelations(self):
        return self.relations
    
    def getString(self,string): #pro elementy
        """
        getString
        
        @param string - element XML stromu
        @return - samotny nazev elementu, pismena jsou mala
        """
        
        string = str(string) #parametr string nebyl string
        string = string[9:]
        string = string.lower() #SQL ma byt case-insensitive
        string = string[0:string.find(" ")] #zustane jen jmeno elementu
        return string
    
    def deepSubmerge(self,root):
        """
        deepSubmerge postupne projde cely strom.
        Pritom tato funkce vytvari slovnik pro datove typy polozek jednotlivych
        vytvarenych tabulek (self.children), slovnik pro pripadne textove
        hodnoty techto tabulek (self.values), slovnik polozek z atributu XML 
        elementu (self.attributes) i slovnik obsahujici jednotlive vzory tabulek
        (self.tables).
        
        @param root - koren stromu ziskany tridou xml.etree.ElementTree
        """
        
        string = ""
        strRoot = self.getString(root)
        if strRoot not in self.tables: 
            self.tables[strRoot] = []
                    
        for child in root:
            strChild = self.getString(child)
            if strChild in self.tables[strRoot]:
                self.tables[strRoot].remove(strChild)
        for child in root:
            strChild = self.getString(child)
            childText = str(child.text)
            
            self.children[strChild] = "INT" #kvuli netext. subelementum 
            self.tables[strRoot].extend([strChild])
            
            #zjistim hodnoty subelementu
            if (child.text != None) and re.findall("\w+", childText, re.U): #(child.text != None) and
                cislaINT = re.findall("\s*[+-]?\d+\s*", childText, re.U)
            
                #RegExp pro FLOAT: \s* #misto pred cislem
                #[+-]? #mozne znamenko
                #(?:\d+ #cela cast
                #(?:\.\d*)?|\.\d+) #desetiny setiny atd
                #(?:[eE][+-]?\d+)?\s* #pripadny exponent
                cislaFLOAT = re.findall("\s*[+-]?(?:\d+(?:\.\d*)?|\.\d+)(?:[eE][+-]?\d+)?\s*", childText, re.U)    
                cislaBIT = re.findall("\s*([01]|false|true)?\s*", childText.lower(), re.U)
                if (
                     cislaBIT and 
                     len(str(cislaBIT[0])) == len(childText) #child.text == None or (
                   ):
                    self.values[strChild] = self.children[strChild] = "BIT"
                elif cislaINT and len(str(cislaINT[0])) == len(childText):
                    self.values[strChild] = self.children[strChild] = "INT"
                elif cislaFLOAT and len(str(cislaFLOAT[0]))==len(childText):
                    self.values[strChild] = self.children[strChild] = "FLOAT"
                else:
                    self.values[strChild] = self.children[strChild] = "NTEXT"
                
            #zjistim atributy        
            attr = child.attrib
            if attr and not self.p.a_flag:
                for key in list(attr.keys()):

                    text = attr[key]
                    cislaINT = re.findall("\s*[+-]?\d+\s*", text, re.U)               
                    cislaFLOAT = re.findall("\s*[+-]?(?:\d+(?:\.\d*)?|\.\d+)(?:[eE][+-]?\d+)?\s*", text, re.U)    
                    cislaBIT = re.findall("\s*([01]|false|true)?\s*", text.lower(), re.U)
                    if strChild not in self.attributes:
                        self.attributes[strChild] = {key:""}

                    if ( text == None or (cislaBIT and 
                         len(str(cislaBIT[0])) == len(text))
                    ):
                        self.attributes[strChild][key] = "BIT"
                    elif ( 
                    cislaINT and len(str(cislaINT[0])) == len(text)
                    ):
                        self.attributes[strChild][key] = "INT"
                    elif ( cislaFLOAT 
                         and len(str(cislaFLOAT[0]))==len(text)
                    ):
                        self.attributes[strChild][key] = "FLOAT"
                    else:
                        self.attributes[strChild][key] = "NVARCHAR"  

            self.deepSubmerge(child) #we need to go deeper
    #END deepSubmerge   
    
    def inTable(self, relTo, relType, table):
        """
        inTables
        
        @param relTo   - nazev cilove tabulky, k niz vede relace
        @param relType - typ relace (N:1, 1:N, M:N)
        @param table   - prohledavana tabulka
        @return - Nachazi se v tabulce 'table' relace na cilovou tabulku?
        """
        
        for key in table:
            value = table[key]
            if (
                relTo == key and
                relType == value
               ):
                return True
        return False
    
    def fillTrans(self, N, NN, table, relTo, tranTo, relationsNew):
        """
        fillTrans

        Pomocna metoda, ktera zaznamenava vyskyt relace N:M.

        @param N - zjistovany typ relace v nadrazene funkci
        @param NN - typ relace zjistovany v teto funkci
        @param table - puvodne prohledavana tabulka (a, R(a,b))
        @param relTo - relace na cilovou tabulku (c, R(b,c))
        @param tranTo - transitivni relace (b, R(a,b))
        @param relationsNew - upravene self.relations      
        """
        
        if ((
             not self.inTable(tranTo,  "N:1", self.relations[table]) and
             not self.inTable(tranTo,  "1:N", self.relations[table])
            )
            and (
                self.inTable(relTo,  NN, self.relations[table]) and
                (
                    self.inTable(tranTo, NN[::-1], self.relations[relTo]) or
                    (
                     N != "N:1" and self.inTable(tranTo, "N:M", self.relations[relTo])
                    )
                )
               )):
                relationsNew[table][tranTo] = "N:M"
                if (not table in relationsNew[tranTo]):
                    relationsNew[tranTo][table] = "N:M"
        
    def fillTransitivity(self, N, table):
        """
        fillTransitivity
        
        Metoda, ktera doplnuje (chybejici) transitivni relace v self.relations,
        v kazdem svem behu vyplnuje jen urcity typ relace.
        
        @param N - typ doplnovane relace (1:N, N:1, N:M)
        @param table - nazev puvodne prohledavane tabulky (a, R(a,c))
        """
        
        relationsNew = copy.deepcopy(self.relations)
        for relTo in self.relations[table]: #N:1 transitivity, R(a,c)
            for tranTo in self.relations[relTo]: #R(c,b)
                if N != "N:M":
                    if (
                        self.inTable(relTo,  N, self.relations[table]) and
                        self.inTable(tranTo, N, self.relations[relTo]) 
                       ):
                        relationsNew[table][tranTo] = N  
                        if (not table in relationsNew[tranTo]):
                            relationsNew[tranTo][table] = N[::-1]   
                else:
                    if ( table != tranTo):
                        self.fillTrans(N, "1:N", table, relTo, tranTo, relationsNew)
                        self.fillTrans(N, "N:1", table, relTo, tranTo, relationsNew)
        self.relations = copy.deepcopy(relationsNew)
                    
    def fillRelations(self):
        """
        fillRelations
        
        Doplnuje chybejici relace, zaznamenane strukturou self.relations.
        V prvnim kroku doplni chybejici zrcadlove relace, pote pro kazdou
        tabulku doplni transitivni relace N:1, 1:N a N:M.
        """
        
        for table in self.relations:
            for relTo in self.relations[table]:
                relType = self.relations[table][relTo]
                #zrcadli existujici relace:            
                if (not self.inTable(table, relType[::-1], self.relations[relTo])):
                    self.relations[relTo][table] = relType[::-1]
        
        for table in self.relations:
            self.fillTransitivity("N:1",table)
            self.fillTransitivity("1:N",table)            
            self.fillTransitivity("N:M",table)
                    
    def printRelations(self):
        """
        printRelations
        
        @return - relace ulozene uvnitr self.relations formou retezce pro
                  vlozeni do textoveho souboru XML.
        """
        
        gap = '    '
        gapp = gap + gap
        content = ''
        for table in self.relations:
            content += gap + '<table name="' + table + '">\n'
            for relTo in self.relations[table]:
                relType = self.relations[table][relTo]
                content += gapp + '<relation to="' + relTo + \
                           '" relation_type="' + relType + '" />\n'
            content += gap + '</table>\n'    
        return '<tables>\n' + content + '<tables>\n'            
             
    def printSubmerge(self,root):
        """
        Podle posledniho slovniku vytvari vysledny retezec,
        kontroluje pritom omezeni na sloupce parametrem --etc=[X] (a z toho 
        vyplyvajici zalezitosti jako jsou zpetne odkazy z dcerinych tabulek),
        generovani polozek atributu (ci jejich vypnuti par. -a nebo jejich
        kolize). Tato funkce rovnez muze byt ovlivnena parametrem -b, ktery
        rusi vytvareni "duplicitnich"(ale ocislovanych) sloupcu. Pripadne chyby
        vedou k ukonceni funkce a programu s navratovou hodnotou 90 a vypisem
        chyboveho hlaseni na stderr.
        
        V pripade aktivniho prepinace -g dochazi k zaznamu netranzitivnich
        relaci typu 1:N a N:1 do slovniku self.relations.
        
        @param root - koren XML stromu
        """
        
        gap = '    '
        string = "CREATE TABLE " + root[1:-1] + "(\n" + gap + "prk_" + root[1:-1]
        string += "_id INT PRIMARY KEY,\n"
        repete = {}
        i = 0
        
        if (
            self.p.g_flag and
            root[1:-1] not in self.relations
           ):  
            self.relations[root[1:-1]] = {root[1:-1]: "1:1"}
                                               
        #pridam subelementy
        for child in self.tables[root]:
            repete[child] = [0,0] #druhe bude pocet opakovani, prvni iteraci
        for child in self.tables[root]:    
            repete[child][1] += 1
        for item in self.tables[root]:
            N = "1:N"
            if self.p.etc_flag:                
                if self.p.etc < repete[item][1]:
                    root_id = root[1:-1] + "_id"                        
                    elem = root_id
                    if item not in self.attributes:
                        self.attributes[item] = {root_id:"INT"}
                    else:
                        self.attributes[item][root_id] = "INT"
                else:
                    N = "N:1"
                    if repete[item][1] > 1:
                        repete[item][0] += 1 #iterace polozky
                        ID = item[0:-1] + str(repete[item][0]) + "'" #ulozim id polozky s cislem
                    else:
                        ID = item                    
                    elem = ID[1:-1] + "_id"
                    string += gap + elem + " INT,\n" #datovy typ                
            else:
                N = "N:1"
                if self.p.b_flag: #opakujici se sloupce se nebudou vypisovat
                    if repete[item][0] < 1:
                        elem = item[1:-1] + "_id"
                        string += gap + elem + " INT,\n"
                        repete[item][0] += 1
                else:
                    if repete[item][1] > 1:
                        repete[item][0] += 1 #iterace polozky
                        ID = item[0:-1] + str(repete[item][0]) + "'" #ulozim id polozky s cislem
                    else:                         
                        ID = item
                    elem = ID[1:-1] + "_id"
                    string += gap + ID[1:-1] + "_id INT,\n" #datovy typ
            if self.p.g_flag:
                NN = N
                if (item[1:-1] in self.relations[root[1:-1]]):
                    if (self.relations[root[1:-1]][item[1:-1]] == N[::-1]):
                        NN = "M:N"
                self.relations[root[1:-1]][item[1:-1]] = NN
            if (root in self.attributes):
                if (elem in self.attributes[root]):
                    printError("Vyskytla se kolize, koncim.\n",90);
        #pridam atributy
        if root in self.attributes:
            for item in self.tables[root]:
                if item in self.attributes[root]:
                    sys.stderr.write("Atribut stejny s potomkem, koncim!")
                    sys.exit(90)
            for key in self.attributes[root]:
                if (key == "value"):
                    sys.stderr.write("Vyskytl se atribut value, koncim!")
                    sys.exit(90)
                string = string + gap + key + " " + self.attributes[root][key] + ",\n"
        
        #pridam (textovou) hodnotu
        if root in self.values: #prida typ textove hodnoty
            string = string + gap + "value " + self.values[root]
        else:
            string = string[0:-2]
        string = string + "\n);\n\n"
        
        self.vysledek += string # toto poputuje ven ze tridy
        
        for child in self.tables[root]:
            if child in self.tables:
                 self.printSubmerge(child) #we need to go deeper
                 del self.tables[child]        
        #END printSubmerge
        
    def submerge(self):            
        """
        submerge
        
        Na zacatku vola metodu deepSubmerge, ktera postupne projde cely strom.
        Pritom vytvari slovnik pro datove typy polozek jednotlivych vytvarenych
        tabulek, slovnik pro pripadne textove hodnoty techto tabulek,
        slovnik polozek z atributu XML elementu i slovnik obsahujici jednotlive
        vzory tabulek.
        
        Pote je volana metoda printSubmerge, ktera vytvari vysledny retezec, 
        podle omezeni danych parametry programu. Pote jsou v pripade prepinace
        -g vyplneny relace a zapsane do retezce vysledku. Pred koncem metody
        je pridavana textova hlavicka, pokud je zadana v parametrech programu.
        
        @return - textovy retezec pro tisk do souboru (SQL nebo XML pro -g)
        """               
        
        self.deepSubmerge(self.root)
        
        repeating = {}
        for item in self.tables[self.getString(self.root)]:    
            if item not in repeating:
                repeating[item] = 0
            else:
                repeating[item] += 1            
            if repeating[item] == 0:
                self.printSubmerge(item)
        
        if self.p.g_flag:
            self.fillRelations()
            self.vysledek = self.printRelations()
        
        if self.p.header_flag:
            self.vysledek = "--" + self.p.header + "\n\n" + self.vysledek
        
        return self.vysledek # a tady to je
    
#END class Submerge

if __name__ == "__main__":

    p = Params()
    p.GetParams() #nactu parametry
    
    try:
        if p.input_flag:
            tree = etree.parse(p.input_file)
        else:
            tree = etree.parse(sys.stdin)
    except IOError:
        printError("Nepodarilo se otevrit vstupni soubor.\n", 2)
    except:
        printError("neocekavana chyba pri parsovani vstupniho souboru\n",91)
            
    strom = Submerge(tree.getroot(),p)    
    
    try:
        if p.output_flag: #merlin.fit.vutbr.cz ma potize s , encoding='utf-8'
            with open(p.output_file, 'w') as outputFile: 
                #vysledek programu se vypise zde:
                result = strom.submerge()      
                outputFile.write(result)
        else:
            with open(1, 'w') as outputFile: #vysledek programu na stdout:
                #vysledek programu se vypise zde:
                result = strom.submerge()
                outputFile.write(result)
    except IOError:
        printError("Nepodarilo se otevrit vystupni soubor.\n", 3)
                

