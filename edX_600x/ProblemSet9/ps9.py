# 6.00 Problem Set 9

import numpy
import random
import pylab
from ps8b_precompiled_27 import *

#
# PROBLEM 1
#
def simNumViruses(steps):
    viruses, resistants = runDrugSimulation(100, 1000, 0.1, 0.05, {'guttagonal': False}, 0.005, steps, (steps+150))
    return viruses, resistants

def simNumViruses2(steps):
    viruses, resistants = runDrugSimulation(100, 1000, 0.1, 0.05, {'guttagonal': False, 'grimpex': False}, 0.005, steps, (steps+150))
    return viruses, resistants

def simulationDelayedTreatment(numTrials):
    """
    Runs simulations and make histograms for problem 1.

    Runs numTrials simulations to show the relationship between delayed
    treatment and patient outcome using a histogram.

    Histograms of final total virus populations are displayed for delays of 300,
    150, 75, 0 timesteps (followed by an additional 150 timesteps of
    simulation).

    numTrials: number of simulation runs to execute (an integer)
    """

    list1 = []
    list2 = []
    list3 = []
    list4 = []
    for _ in range(numTrials):
        viruses, resistants = simNumViruses(300)
        list1.append(viruses[-1])
        viruses, resistants = simNumViruses(150)
        list2.append(viruses[-1])
        viruses, resistants = simNumViruses(75)
        list3.append(viruses[-1])
        viruses, resistants = simNumViruses(0)
        list4.append(viruses[-1])
        
    print list1
    print list2
    print list3
    print list4
    
    #histogram
    pylab.subplot(221)
    pylab.hist(list1,bins=50)
    pylab.title('Virus Treatment - 300 steps delay')
    pylab.xlabel('Final virus population')
    pylab.ylabel('Number of trials')

    pylab.subplot(222)
    pylab.hist(list2,bins=50)
    pylab.title('Virus Treatment - 150 steps delay')
    pylab.xlabel('Final virus population')
    pylab.ylabel('Number of trials')

    pylab.subplot(223)
    pylab.hist(list3,bins=50)
    pylab.title('Virus Treatment - 75 steps delay')
    pylab.xlabel('Final virus population')
    pylab.ylabel('Number of trials')

    pylab.subplot(224)
    pylab.hist(list4,bins=50)
    pylab.title('Virus Treatment - 0 steps delay')
    pylab.xlabel('Final virus population')
    pylab.ylabel('Number of trials')

    pylab.tight_layout()
    pylab.show()



#
# PROBLEM 2
#
def simulationTwoDrugsDelayedTreatment(numTrials):
    """
    Runs simulations and make histograms for problem 2.

    Runs numTrials simulations to show the relationship between administration
    of multiple drugs and patient outcome.

    Histograms of final total virus populations are displayed for lag times of
    300, 150, 75, 0 timesteps between adding drugs (followed by an additional
    150 timesteps of simulation).

    numTrials: number of simulation runs to execute (an integer)
    """

    list1 = []
    list2 = []
    list3 = []
    list4 = []
    for i in range(numTrials):
        viruses, resistants = simNumViruses2(300)
        list1.append(viruses[-1])
        viruses, resistants = simNumViruses2(150)
        list2.append(viruses[-1])
        viruses, resistants = simNumViruses2(75)
        list3.append(viruses[-1])
        viruses, resistants = simNumViruses2(0)
        list4.append(viruses[-1])
        print i
        
    print list1
    print list2
    print list3
    print list4
    
    #histogram
    pylab.subplot(221)
    pylab.hist(list1,bins=50)
    pylab.title('Virus Treatment - 300 steps delay')
    pylab.xlabel('Final virus population')
    pylab.ylabel('Number of trials')

    pylab.subplot(222)
    pylab.hist(list2,bins=50)
    pylab.title('Virus Treatment - 150 steps delay')
    pylab.xlabel('Final virus population')
    pylab.ylabel('Number of trials')

    pylab.subplot(223)
    pylab.hist(list3,bins=50)
    pylab.title('Virus Treatment - 75 steps delay')
    pylab.xlabel('Final virus population')
    pylab.ylabel('Number of trials')

    pylab.subplot(224)
    pylab.hist(list4,bins=50)
    pylab.title('Virus Treatment - 0 steps delay')
    pylab.xlabel('Final virus population')
    pylab.ylabel('Number of trials')

    pylab.tight_layout()
    pylab.show()
