#!/bin/sh

str="http://www.cnb.cz/cs/financni_trhy/devizovy_trh/kurzy_devizoveho_trhu/denni_kurz.txt?date="

d=$(date '+%d.%m.%Y')
url=$str.$d
page="$(wget -q -O - $url)"

USD=`echo "$page" | grep USA`
US=`echo $USD | cut -d'|' -f 5`

EUR=`echo "$page" | grep EUR`
EU=`echo $EUR | cut -d'|' -f 5`

GBP=`echo "$page" | grep GBP`
GB=`echo $GBP | cut -d'|' -f 5`

JPY=`echo "$page" | grep JPY`
JP=`echo $JPY | cut -d'|' -f 5`

echo ""
echo "      AKTUALNI KURZ CNB:"
echo "      USD: "$US
echo "      EUR: "$EU
echo "      GBP: "$GB
echo "      YEN: "$JP
