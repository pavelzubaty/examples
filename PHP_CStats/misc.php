<?php

//CST:xzubat02

/**
*  C stats - miscellaneous functions, constants, etc.
*
*/

    //mb_internal_encoding("utf-8");
    mb_internal_encoding("ISO-8859-2");
    
    const EXIT_SUCCESS = 0;
    const errParam = 1;
    const errInput = 2;
    const errOutput = 3;
    const errFileUnreadable = 21;
    const errOther = 101;
    
    $help =  "CST: C Stats\n"
           . "\n"
           . "This script analyses ISO C99 compliant* C language source code\n"
           . "before returning statistics of comments, keywords, operators \n"
           . "and strings in the set format.\n"
           . "______________________________________________________________\n"
           . "PARAMETERS:\n"
           . "\n"
           . "  --help - you are reading it now\n"
           . "\n"
           . "  --input=fileordir - input file or directory with source code\n"
           . "    in C language. Script works properly with files (.c, .h) \n"
           . "    encoded in ISO-8859-2. If a directory is set, all files \n"
           . "    are processed including files in subdirectories. This\n"
           . "    parameter is optional. For it not being set current\n"
           . "    directory is used instead.\n"
           . "\n"
           . "  --nosubdir - optional, files in subdirectories are skipped.\n"
           . "    Combination with --input=file where file is being set\n"
           . "    is invalid.\n"
           . "\n"
           . "  --output=filename - text file where output will be printed.\n"
           . "\n"
           . "  -k - keywords (outside of comments and strings) for each\n"
           . "       source file and in total.\n"
           . "\n"
           . "  -o - simple operators outside comments, character literals\n"
           . "       and strings in every source file and total. Simple\n"
           . "       operator is defined as a predefined fixed pattern\n"
           . "       of non-alphanumeric characters\n"
           . "\n"
           . "  -i - identificators (outside parameters, character literals\n"
           . "       and strings) in each source file and in total. Keywords\n"
           . "       are not included.\n"
           . "\n"
           . "  -w=pattern - counts every occurence of exact string pattern\n"
           . "       in all source files and prints non-overlapping\n"
           . "       occurences per file and in total. The search is\n"
           . "       concluded everywhere in the file (including comments,)\n"
           . "       macros, character literals and strings). It's also\n"
           . "       case-sensitive.\n"
           . "\n"
           . "  -c - characters in comments including comment start/end\n"
           . "       pattern (//, /* and */) per file and in total. Endline\n"
           . "       and multiline backslash are included.\n"
           . "\n"
           . "  -p - in combination with previous parameters only file names\n"
           . "       will be printed (without absolute path).\n"
           . "\n"
           . "Parameters -k, -o, -i, -w, -c, --help cannot be combined.\n"
           . "______________________________________________________________\n"
           . "* ISO/IEC 9899:TCC http://www.open-std.org/jtc1/sc22/wg14/www/docs/n1256.pdf \n"; 
           
    $operators = array("<<=", ">>=", "<=", ">=", "+=", "-=", 
                       "/=", "%=", "*=", "&=", "|=", "^=", 
                       "!=", "==", "&&", "||", "<<", ">>",
                       "++", "--", "->", "~", ".", "+","-",
                       "!", "&", "/", "%", "<", ">", "^",  
                       "|", "=", "*"
                      );
    $operands = array(
                      "++", "--", "=", "+=", "-=", "*=",
                      "%=", "<<=", ">>=", "&=", "^=", "|="
                      );                  
    $keywords = array("_Imaginary","continue","register","default",
                      "typedef","unsigned","volatile","_Complex",
                      "restrict","double","extern","inline",
                      "return","signed","sizeof","static",
                      "struct","switch","while","_Bool",
                      "short","union","else","const",
                      "break","auto","case","char",
                      "void","float","enum","long",
                      "goto","int","for","if","do",
                      );    
    $datatypes = array("unsigned","_Complex","double","signed",
                       "short","float","_Bool","int","void","char","long"
                      );         
    
    /**
    *  function printError()
    *
    *  Prints error message $str to standard error output then terminates
    *  script with error code $code.
    *
    *  @param $str - error message
    *  @param $code - error exit code
    */   
    function printError($str,$code) {
        $str .= "\n";
        file_put_contents("php://stderr", $str);
        exit($code);        
    } 
    
    /**
    *  function openFile()
    *  
    *  Opens output file $file or terminates script if unsuccessfull.
    *
    *  @param $file - file name to be opened
    *  @return $output - opened file handler
    */
    function openFile($file) {
        if (empty($file))
            printError("Nepodarilo se otevrit vystupni soubor.", errOutput);
        if (file_exists($file) && $file != "php://stdout" && !is_writable($file))
            printError("Nepodarilo se otevrit vystupni soubor.", errOutput);
        $err = error_reporting();
        error_reporting(0); 
        $output = fopen($file, "w");  
        error_reporting($err);  
        if (!$output)
            printError("Nepodarilo se otevrit vystupni soubor.", errOutput);
        return $output;    
    }

    /**
    *  function openFileInput()
    *  
    *  Opens input file $file or terminates script if unsuccessfull.
    *
    *  @param $file - file name to be opened
    *  @return $output - opened file handler
    */    
    function openFileInput($file) {
        if (empty($file))
            printError("Nepodarilo se otevrit vstupni soubor.", errInput);
        if (!is_readable($file))
            printError("Nepodarilo se otevrit vstupni soubor.", errFileUnreadable);
        $err = error_reporting();
        error_reporting(0);
        $fin = fopen($file,"r");
        error_reporting($err);
        if (!$fin)            
            printError("Nepodarilo se otevrit vstupni soubor.", errInput);
        return $fin;    
    }

    /**
    *  function printStats()
    *  
    *  Prints statistics stored in $stats associative array
    *  @param $fout - name of the output file
    *  @param $total - total amount of occurences
    *  @param $stats - associative array with elements like this ("dir/file","100")
    */
    function sortByFirst($a, $b) {
        return strcmp($a[0],$b[0]);
    }
    function printStats($fout,$total,$stats) {  
        $output = openFile($fout);
              
        $arr = array();
        /*foreach ($stats as $stat => $row) {
	        $arr[$stat] = $row[0];
	        $arr[$stat][1] = (string)$row[1];
        }
        var_dump($arr);
        array_multisort($arr, SORT_STRING, $stats);
        $stats = array();
        $stats = $arr; */
        
        usort($stats, 'sortByFirst');
        $length = 7;
        $keyLength = 0;
        foreach($stats as $value) {
            $len = mb_strlen($value[0]);
            $klen = mb_strlen((string)$value[1]);
            if ($len >= $length) $length = $len;
            if ($klen >= $keyLength) $keyLength = $klen;    
        }
        
        $length = $length + $keyLength;            
        foreach($stats as $value) {
            $len = $length - mb_strlen($value[0]) - mb_strlen((string)$value[1]);
            $gap = str_repeat(" ", $len);
            $str = $value[0] . $gap . " ".$value[1] . "\n";
            fprintf($output,$str);
        }
        
        $total = (string)$total;
        $len = $length - strlen($total) - 7;
        if ($len < 0) $len = 0;
            
        $gap = str_repeat(" ", $len);
        $str = "CELKEM: " . $gap . $total . "\n";
        
        fprintf($output,$str);
        fclose($output);
    }
    
    /**
    *  function printStats()
    *  
    *  Prints statistics stored in $stats associative array
    *  @param $fout - name of the output file
    *  @param $total - total amount of occurences
    *  @param $stats - associative array with elements like this ("dir/file","100")
    */
    function printStatsOld($fout,$total,$stats) {  
        $output = openFile($fout);
              
        $arr = array();
        foreach($stats as $stat) {            
            $arr[$stat[0]] = (string)$stat[1];
        }
        $stats = array();
        $stats = $arr;  
        ksort($stats);
        $length = 0;
        $keyLength = 7;
        foreach($stats as $key => $value) {
            $len = mb_strlen($value);
            $klen = mb_strlen($key);
            if ($len >= $length) $length = $len;
            if ($klen >= $keyLength) $keyLength = $klen;    
        }
        
        $length = $length + $keyLength;            
        foreach($stats as $key => $value) {
            $len = $length - mb_strlen($key) - mb_strlen($value);
            $gap = str_repeat(" ", $len);
            $str = $key . $gap . " ".$value . "\n";
            fprintf($output,$str);
        }
        
        $total = (string)$total;
        $len = $length - strlen($total) - 7;
        if ($len < 0) $len = 0;
            
        $gap = str_repeat(" ", $len);
        $str = "CELKEM: " . $gap . $total . "\n";
        
        fprintf($output,$str);
        fclose($output);
    }
    
    /**
    *  is there some $ch before first other char $oth?
    */
    function testFor($ch,$buffer,$oth) {
        $other = "'".$oth."'";
        $buffer = str_replace($other,"",$buffer); 
        
        $char = "'".$ch."'";
        $buffer = str_replace($char,"",$buffer);        
        
        $charPos = mb_strpos($buffer,$ch);
        
        $othPos = mb_strpos($buffer,$oth,$charPos);
        if($othPos !== false)
            return true;
          
        return false;
    }
?>
