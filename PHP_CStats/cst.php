<?php

//CST:xzubat02

/**
*  C Stats - main module
*
*  This script analyses ISO C99 compliant* C language source code
*  before returning statistics of comments, keywords, operators
*  and strings in the set format.
*
*  First parameters are processed (see --help or $help
*/

    include "stats.php";
        
    /*MAIN*/
       
    $par = new Params($argc,$help);
    $stats = new Stats($par,$operators,$keywords,$datatypes);
    $stats->getFileStats();
    $stats->printStats();
?>
