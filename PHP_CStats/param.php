<?php
    
//CST:xzubat02
    
/**
*  C Stats - class Params
*  Main script (cst.php) arguments are processed then stored in this class.
*
*/
    include "misc.php";
    
    class Params {
        public $inFile = "";
        public $outFile = "php://stdout";
        public $files = array();
        
        private $nosubdir = false;     
        private $k = false;
        private $o = false;
        private $i = false;
        private $w = false;
        private $pattern = "";
        private $c = false;
        private $p = false;
               
        public function isDir() {
            if (is_dir($this->inFile)) return true;
            else return false;       
        }
        
        public function noSubDir()  { return $this->nosubdir; }  
        public function keywords()  { return $this->k; }
        public function operators() { return $this->o; }
        public function identificators() { return $this->i; }
        public function search() { return $this->w; } 
        public function pattern() { return $this->pattern; }
        public function charsInComments() { return $this->c; }
        public function path() { return $this->p; }
        
        /**
           OTHER CLASS METHODS:
           private function loadFileList($dir);
           private function loadFileList2($dir);
           public function __construct($argc);
        */
        
        /**
        *  function loadFileList()
        *
        *  Returns name of every file under the uppermost directory,
        *  including files in subdirectories.
        *
        *  @param $dir - name of the uppermost directory
        *  @return - array of all files under @param $dir directory
        */                        
        private function loadFileList($dir) {
            $files = glob("$dir/*.{c,h}",GLOB_BRACE);
            $dirs = glob("$dir/*",GLOB_ONLYDIR);
                     
            foreach ($dirs as $dir) {
                $files = array_merge($files, $this->loadFileList($dir));
            }  
                    
            return $files;
        }
        
        /**
        *  function loadFileList2()
        *
        *  Returns name of every file in the $dir itself,
        *  but not in subdirectories.
        *
        *  @return - array of all files under @param $dir directory
        */ 
        private function loadFileList2($dir) { //only current dir
            return glob("$dir/*.{c,h}",GLOB_BRACE);
        }
                
        /**
        *  function __construct()
        *
        *  Param constructor. Processes main script arguments. First it checks
        *  for unknown parameters and invalid combinations. If any are found,
        *  terminates program writing error report to stderr before exiting 
        *  with appropriate error code (see misc.php). In case of --help $help
        *  is printed before exiting succesfully. Otherwise new arguments are
        *  stored for further use (inside class Stats object, see stats.php).
        *
        *  @param $argc - main script arguments count
        *  @param $help - string to be printed in case of --help
        */        
        public function __construct($argc, $help) {
            $this->inFile = getcwd();
            
            $longopts = array(
                "help",      // print help
                "input:",    // input file or directory
                "nosubdir",  // only current directory, no subdirectories
                "output:",   // output textfile in ISO-8859-2
            );
            $shortopts = "";
            $shortopts .= "k";  // occurences of keywords
            $shortopts .= "o";  // occurences of simple operators
            $shortopts .= "i";  // occurences of identificators
            $shortopts .= "w:"; // -w=pattern occurences of "pattern" which is string
            $shortopts .= "c";  // characters in comments as total
            $shortopts .= "p";  // files name print without fullpath
                
            $options = getopt($shortopts, $longopts);
                
            if (sizeof($options) < ($argc - 1))
                printError("Neznamy parametr nebo opakovany vyskyt nektereho parametru",errParam);
                     
            foreach ($options as $opt) {
                if (is_array($opt)) {        
                    printError("Opakovany vyskyt nektereho parametru.",errParam);
                }        
            }
            
            if (isset($options["help"])) {
                if ($argc > 2) 
                    printError("Parametr --help nelze kombinovat. Napoveda viz cst.php --help",errParam);
                        
                print($help);
                exit(EXIT_SUCCESS);
            }
            else {
                $this->nosubdir = isset($options["nosubdir"]);
                
                if (isset($options["input"])) {
                    $this->inFile = $options["input"];                    
                    if ($this->noSubDir() && !$this->isDir())
                        printError("Parametry nosubdir a input se zadanim konkretniho souboru nelze kombinovat.",errParam);                     
                }
                if (isset($options["output"]))
                     $this->outFile = $options["output"];                     
                if ($this->isDir()) {
                     if ($this->noSubDir())
                        $this->files = $this->loadFileList2($this->inFile);
                     else
                        $this->files = $this->loadFileList($this->inFile);
                }
                else
                    array_push($this->files, $this->inFile);
                    
                
                $this->p = isset($options["p"]);
                
                $check = 0;
                if ($this->k = isset($options["k"])) $check++;
                if ($this->o = isset($options["o"])) $check++;
                if ($this->i = isset($options["i"])) $check++;
                if ($this->w = isset($options["w"])) {
                    $check++;
                    $this->pattern = $options["w"];
                }
                if ($this->c = isset($options["c"])) $check++;
                
                if ($check > 1) printError("Parametry -k, -o, -i, -w, -c nelze kombinovat.",errParam);                                                  
            } 
        }    //__construct   
    } //class Params    
?>
