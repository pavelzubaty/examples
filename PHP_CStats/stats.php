<?php

//CST:xzubat02

/**
*  C Stats - core module, contains class Stats and several constants.
*
*  class Stats - core code of the C Stats.
*  It's a sum of functions to return statistics of all ISO C compliant files
*  according to arguments (previously stored in Param object) the cst.php script 
*  is being run with.
*  NOTE: class needs companion modules param.php and misc.php to work as intended.
*  NOTE2: new values of some functions are being returned by reference.
*  This might not be mentioned in their description. 
*  Constructor parameters:
*     @param $par - contains class Param object (see param.php) which contains
*                   processed arguments of the main script (cst.php)
*     @param $operators - ISO C operators stored as elements of this array
*                         of strings
*     @param $keywords  - ISO C keywords stored as elements of this array
*                         of strings
*     @param $datatypes - ISO C defined datatypes stored as elements of this
*                         array of strings
*/

    include "param.php";
    
    const startLineComment = "/\/\/.*$/";
    const startComment = "/\/\*.*$/";
    const endComment = "/\*\/.*$/";
    const startMacro = "/^[\s]*\#.*$/";
    const multiLine = "/([\\\]\s*)$/";
    const singleQuote = "/\'.*$/";
    const doubleQuote = '/\".*$/';
    
    /**
    *  class Stats - core code of the C Stats.
    *  It's a sum of functions to return statistics of all ISO C compliant files
    *  according to arguments (previously stored in Param object) the cst.php
    *  script is being run with.
    *  Constructor parameters:
    *     @param $par - contains class Param object (see param.php) which contains
    *                   processed arguments of the main script (cst.php)
    *     @param $operators - ISO C operators stored as elements of this array
    *                         of strings
    *     @param $keywords  - ISO C keywords stored as elements of this array
    *                         of strings
    *     @param $datatypes - ISO C defined datatypes stored as elements of this
    *                         array of strings
    */
    class Stats {
        
        private $par = NULL;
        private $files = array();
        private $stats = array();
        private $chars = 0;
        private $output = NULL;        
        
        private $operators = array();
        private $keywords = array();
        private $datatypes = array();
        
        private $isComment = false;
        private $isLineComment = false; // //
        private $isString = false; // "
        
        private $isBracket = false; // (
        
        /**
        CLASS METHODS:

        public function __construct($par,$operators,$keywords,$datatypes);
        public function Chars();
        public function Stats();
        public function printStats();

        public function getFileStats();
        private function _getFileStats($file);

        private function _countIdentificators(&$buffer,&$chars);
        private function _removeOperators(&$buffer);
        private function _removeStringLiterals(&$buffer);
        private function _countKeyWords(&$buffer,&$chars);
        private function _countOperators(&$buffer,&$chars);
        private function _removeMisc(&$buffer);
        private function _removeConstants(&$buffer);

        private function _removeKeywordsHandler(&$buffer);
        private function _removeKeywords(&$buffer);
        private function _removeAsterisk(&$buffer);
        private function _removeAsteriskBetween(&$buffer,$brA,$brB);
        private function _isLeftSide(&$buffer,$pos);

        private function _removeComments(&$buffer);
        private function _removeCom($buffer);

        private function _searchFile($par,$file);

        private function _parComments($buffer, $chars);
        private function _isMultiLine($buffer);
        private function _removeStringsHandler(&$buffer);
        private function _removeStrings($buffer);
        private function _bufLen($chars,$buffer);
        private function _getStartComment(&$buffer,$mark);
        private function _getEndComment(&$buffer, &$chars);
        */
        
        /**
        *  See getFileStats() for further description.
        */
        public function Chars() { return $this->chars; }
        public function Stats() { return $this->stats; }
        
        /**
        *  function __construct() - Stats constructor
        *  
        *  The object of Stats class is initialized with following
        *  parameters:
        *     @param $par - contains class Param object (see param.php) which
        *                   contains processed arguments of the main script
        *     @param $operators - ISO C operators stored as elements of this array
        *                         of strings
        *     @param $keywords  - ISO C keywords stored as elements of this array
        *                         of strings
        *     @param $datatypes - ISO C defined datatypes stored as elements
        *                         of this array of strings
        */
        public function __construct($par,$operators,$keywords,$datatypes) {           
            $fout = openFile($par->outFile);
            fclose($fout);
            
            $this->par = $par;
            $this->output = $par->outFile;
            foreach($par->files as $file) {
                array_push($this->files, realpath($file));
            }          
            $this->operators = $operators;
            $this->keywords = $keywords;      
            $this->datatypes = $datatypes;           
        }
        
        /**
        * function printStats()
        *
        * This fuction is handler of printStats function defined outside
        * of the Stats class. This extern function prints statistics on
        * the standard (error) output before ending whole program with
        * appropriate termination value (either 0 for success or appropriate
        * higher value marking failure).
        */        
        public function printStats() {
            printStats($this->output,$this->chars,$this->stats);
        }
               
        
        /**
        * function _getEndComment()
        *
        * Gets number of character from the start of the $buffer to the first
        * occurence of the end-comment pattern (asterisk and slash). Pattern
        * is included as two characters.
        * @param &$buffer - string to be processed
        * @param &$chars - number of characters in comments so far 
        */
        private function _getEndComment(&$buffer, &$chars) {            
            $pos = mb_strpos($buffer,"*/",0) + 2;
            $buffer = mb_substr($buffer,$pos,mb_strlen($buffer));
            $chars += $pos;            
        }
        
        /**
        *  function _getStartComment()
        *
        *  Removes everything from the input $buffer before the $mark pattern.
        *  @param &$buffer - string to be processed
        *  @param $mark - string pattern denoting start of the comment 
        */
        private function _getStartComment(&$buffer,$mark) {
            $pos = mb_strpos($buffer,$mark,0);
            $buffer = mb_substr($buffer,$pos,mb_strlen($buffer));
        }
        
        /**
        *  function _bufLen()
        *
        *  Adds $buffer length to $chars value.
        *  @param $buffer - string to be used
        *  @param &$chars - number of characters
        *  @return - $chars value incremented by length of the $buffer
        */
        private function _bufLen($chars,$buffer) {
            $chars += mb_strlen($buffer);
            return $chars;
        }
        
        /**
        *  function _removeStrings()
        *
        *  Removes strings from the input ISO C code with the exception of 
        *  strings in comments. Both types of comments (line and block comments),
        *  even in their multiline version (line comment extended by backslash \)
        *  are considered. Because this function is being called from the handler
        *  _removeStringsHandler(), lines inside of multiline comments are skipped
        *  altogether and function _removeStrings() isn't even called.
        * 
        *  First function _removeStrings checks for first occurence of double quote
        *  (skipping quotes inside '"' literals), line comment starting pattern
        *  and block comment starting pattern. In case of multiline strings
        *  ending double quote is being found, otherwise whole line is erased.
        *  If there is one double quote outside of comments, following lines are
        *  marked as multiline string. Everything between two double quotes is 
        *  being erased. 
        *
        *  Substring after closing double quote is processed by recursive call
        *  of _removeStrings(). Same recursive call is being applied for substring
        *  following end of the block comment.
        *
        *  PARAMETERS:
        *  @param $buffer - string to be processed
        *  @return - modified $buffer, ie. input string without comments ""  
        */
        private function _removeStrings($buffer) {
            $start = mb_strpos($buffer,'"',0);
            $line = mb_strpos($buffer,'//',0);
            $com = mb_strpos($buffer,'/*',0);     
            
            if ($start>0 && $start<mb_strlen($buffer))
                if ($buffer[$start+1] == "'" && $buffer[$start-1] == "'") // a='"'
                    $start = mb_strpos($buffer,'"',$start+1);
                                 
            if ($this->isString) {                
                if ($start === false)
                    return "";
                
                $buffer = mb_substr($buffer,$start+1,mb_strlen($buffer));
                $this->isString = false;
                return $buffer;
            }
                        
            if ($start === false) //no double quote found
                return $buffer;
            
            if ($line !== false && $line < $start
                && ($line < $com || $com === false)) {
                return $buffer; // // /* "
            }
            
            if ($com !== false && $com < $start) {
                $comEnd = mb_strpos($buffer,'*/',$com);
                if ($comEnd === false) {
                    return $buffer; // /* "
                }
                else {  // /* " */ blabla
                    $buf = mb_substr($buffer,$comEnd+2,mb_strlen($buffer));
                    $buf = $this->_removeStrings($buf);
                    $buffer = mb_substr($buffer,0,$comEnd+2) . $buf;
                    return $buffer;
                }
            }
            
            if ($start+1 <= mb_strlen($buffer))
                $end = mb_strpos($buffer,'"',$start+1);
            else
                $end = false;
                
            if ($end !== false) { // " " blabla
                $this->isString = false;
                $buf = mb_substr($buffer,$end+1,mb_strlen($buffer));
                $b = mb_substr($buffer,0,$start);
                $buffer = $b . $this->_removeStrings($buf);
                return $buffer;
            }
            
            $this->isString = true;
            return mb_substr($buffer,0,$start) . "\n";
        }
        
        /**
        *  function _removeStringsHandler()
        *
        *  Calls _removeStrings() for lines outside of multiline comments and
        *  the rest of the line after the multiline block comment.
        *
        *  @param &$buffer - string to be processed
        */ 
        private function _removeStringsHandler(&$buffer) {
            if (!$this->isComment && !$this->isLineComment) {
                $buffer = $this->_removeStrings($buffer);                
            }
            else if (!$this->isLineComment) {
                $pos = mb_strpos($buffer,"*/",0);
                if ($pos !== false) {
                    $buf = mb_substr($buffer,$pos+2,mb_strlen($buffer));
                    $buf = $this->_removeStrings($buf);
                    $buffer = mb_substr($buffer,0,$pos+2) . $buf;
                }
            }            
        }
        
        /**
        *  function _isMultiLine()
        *
        *  Checks if the line ends with \ (before the endline character).
        *
        *  @param $buffer - string to be used 
        *  @return - is there \ just before the line ends?
        */
        private function _isMultiLine($buffer) {
                $strlen = (mb_strlen($buffer) > 1) ? mb_strlen($buffer)-2 : 0;
                $pos = mb_strpos($buffer,"\\",$strlen);
                return ($pos !== false) ? true : false;
        }
        
        /**
        *  function _parComments()
        *
        *  Counts number of characters in comments including starting (and ending)
        *  comment pattern. In case of multiline comments whole line length is 
        *  counted in unless block multiline comment ends there. Normally only
        *  characters between / *   * / or after // are added to the length.
        *  
        *  @param $buffer - string to be used
        *  @param $chars - number of characters  
        *  @return - number of comment characters in the $buffer 
        */        
        private function _parComments($buffer, $chars) {                           
            if ($this->isComment) { // block comment                
                if (preg_match(endComment, $buffer)) { //   */
                    $this->isComment = false;
                    $this->_getEndComment($buffer, $chars);
                }
                else
                    return $this->_bufLen($chars,$buffer);
            }      
            else if ($this->isLineComment) { //multiline line comment // blabla \ 
                $this->isLineComment = $this->_isMultiLine($buffer);
                return $this->_bufLen($chars,$buffer);
            }      
            
            while (preg_match(startComment, $buffer)) { // /* */ /* /* */
                if (mb_strpos($buffer,"//",0) < mb_strpos($buffer,"/*",0))
                    break;
                $this->isComment = true;
                $this->_getStartComment($buffer,"/*");
                
                if (preg_match(endComment, $buffer)) { // /*  */
                    $this->isComment = false;
                    $this->_getEndComment($buffer, $chars);
                }                    
                else // /*        \n
                    return $this->_bufLen($chars,$buffer);
            }
            
            if (preg_match(startLineComment,$buffer)) { // //
                $this->_getStartComment($buffer,"//");
                $this->isLineComment = $this->_isMultiLine($buffer);
                return $this->_bufLen($chars,$buffer);
            }
            return $chars;
        }        
        
        /**
        *  function _searchFile()
        *  
        *  Counts all occurences of string pattern $par inside $file.
        *
        *  @param $par - pattern to be searched for
        *  @param $file - file where the search will occur
        *  @return - number of occurences in the file
        */
        private function _searchFile($par,$file) {
            if (empty($par))
                printError("Nelze vyhledat retezec nulove delky.",errOther);
                
            $len = mb_strlen($par);
            $chars = 0;
            if(($buffer = file_get_contents($file)) == false)
                printError("Selhalo cteni ze vstupniho souboru.",errInput);
            
            while (!empty($buffer)) {
                $pos = mb_strpos($buffer,$par,0);                    
                    
                if ($pos) {
                   $chars++;
                   $pos += $len;
                   $buffer = mb_substr($buffer,$pos,mb_strlen($buffer));
                }
                else
                    break;
            }
            return $chars;                
        }
        
        /**
        *  function _removeCom()
        *
        *  Removes every comment from the $buffer and marks multiline comments.
        *
        *  @param $buffer - string to be used 
        *  @return - modified $buffer, ie. without comments
        */
        private function _removeCom($buffer) {
            $start = mb_strpos($buffer,"/*");
            $pos = mb_strpos($buffer,"//");
            $end = mb_strpos($buffer,"*/");
            if (($start !== false) && (($start < $pos) || ($pos === false))) {
                if ($end === false) {
                    $this->isComment = true;
                    $buffer = mb_substr($buffer,0,$start);                    
                }
                else {
                    $this->isComment = false;
                    $buf = mb_substr($buffer,$end+2,mb_strlen($buffer));
                    $buf = $this->_removeCom($buf);
                    $buffer = mb_substr($buffer,0,$start) . $buf;
                }
            }
            else if ($pos !== false) {            
                $this->isLineComment = $this->_isMultiLine($buffer);
                $buffer = mb_substr($buffer,0,$pos);
            }
            
            return $buffer;
        }
        
        /**
        *  function _removeComments()
        *
        *  _removeCom handler - calls _removeCom for every line outside of
        *  multiline comments and the rest of line where multiline block
        *  comment ends.
        *
        *  @param &$buffer - string to be processed
        */
        private function _removeComments(&$buffer) {
            if ($this->isLineComment) {
                $this->isLineComment = $this->_isMultiLine($buffer);
                $buffer = "";
            }            
            else if ($this->isComment) {
                $pos = mb_strpos($buffer,"*/");
                if ($pos !== false) { 
                    $this->isComment = false;               
                    $buffer = mb_substr($buffer,$pos+2,mb_strlen($buffer));
                    $buffer = $this->_removeCom($buffer);
                }
                else $buffer = "";
            }
            else {
                $buffer = $this->_removeCom($buffer);
            }
        }                         
        
        /**
        *  function _isLeftSide()
        *
        *  Searches for = Returns false if there is no = found.
        *
        *  @param &$buffer - string to be used
        *  @param $pos - starting position of the search 
        *  @return - position of = Returns false if no = is found.
        */
        private function _isLeftSide(&$buffer,$pos) {
             while (1) { //is it left side?
                 $q = mb_strpos($buffer,"==",$pos);
                 $pos = mb_strpos($buffer,"=",$pos);
                 if ($q !== false && $q == $pos) {
                    $pos += 2;
                    if ($pos >= mb_strlen($buffer))
                         $pos = false;
                 }
                 if ($q === false || $pos === false)
                    break;                 
             }
             return $pos;
        }
        
        /**
        *  function _removeAsteriskBetween()
        *
        *  Divides &$buffer into three parts to be processed by _removeAsterisk()
        *  according to position of ( and ) brackets.
        * 
        *  @param &$buffer - string to be processed
        *  @param $brA - starting bracket
        *  @param $brB - ending bracket
        */
        private function _removeAsteriskBetween(&$buffer,$brA,$brB) {            
            if ($brB !== false) {
                $b1 = mb_substr($buffer,0,$brA);
                $b2 = mb_substr($buffer,$brA+1,$brB-$brA-1);                
                $b3 = mb_substr($buffer,$brB+1);
                $this->_removeAsterisk($b1);                
                $this->_removeAsterisk($b2);
                $this->_removeAsterisk($b3);
                $b1 = $b1 . "(";
                $b2 = $b2 . ")";
            }            
            else {
                $b1 = mb_substr($buffer,0,$brA);
                $b2 = mb_substr($buffer,$brA+1);
                $this->_removeAsterisk($b1);
                $this->_removeAsterisk($b2);
                $b1 = $b1 . "(";
                $b3 = "";
            }            
            $buffer = $b1. $b2 . $b3;
        }
        
        /**
        *  function _removeAsterisk()
        *
        *  Removes every asterisk which is not simple operator. If there is
        *  datatype before asterisk, every asterisk afterwards is removed
        *  up to = or end of the buffer (closing bracket in the case of
        *  type casting or endline for declaration).
        *
        *  @param &$buffer - string to be processed
        */                       
        private function _removeAsterisk(&$buffer) {
            //remove the asterisk
            $pos = 0;
            while (($star = mb_strpos($buffer,"*",$pos)) !== false) {
                foreach ($this->datatypes as $datatype) {
                    $type = mb_strpos($buffer,$datatype,$pos);
                    if ($type === false)
                        continue;    
                    $brA = mb_strpos($buffer,"(",$type);
                    $brB = mb_strpos($buffer,")",$type);
                    $eq = $this->_isLeftSide(&$buffer,$type);
                          
                    if (($star > $type) && ($type !== false)) {
                        if ($eq !== false) { // int * =
                            if ((($eq > $brB) && ($brB > $brA)
                                    && (($brB !== false) && ($brA !== false)))
                                 || ($brB === false && $brA === false)) {
                                // int (*) =  ; int * =
                                $is = "=";
                                $buf = mb_substr($buffer,0,$eq);
                                if (mb_substr($buf,-1) == "*")
                                    $is = "*=";
                                                                    
                                $b = mb_substr($buffer,$eq+1);
                                $this->_removeKeywords($buf);
                                $this->_removeKeywords($b);
                                $buffer = $buf. $is . $b; 
                            }
                        }
                        else { //$eq === false                               
                            if ($brA === false || $star < $brA) {
                                $buf = mb_substr($buffer,0,$star);
                                // *(*(char *)p)--;
                                if (($prev = mb_strrpos($buf,"(")) !== false) {
                                $this->_removeAsteriskBetween($buffer,$prev,$brB);
                                
                                }
                                else { // remove asterisks except simple operators
                                    $b = mb_substr($buffer,$star+1);
                                    $buf = str_replace("*","",$buf);
                                    $buf = str_replace($datatype,"",$buf);
                                    $buffer = $buf. "" . $b;
                                }
                            }                                
                            else if (($brA !== false && $brA < $brB) // (dfdf)
                                 || ($brA !== false && $brB === false)) // (dsf
                                 $this->_removeAsteriskBetween($buffer,$brA,$brB);
                        } //$eq === false
                    } //if ($star > type)...
                } //foreach
                    
                if ($star !== false)
                   $pos = ($star+1 < mb_strlen($buffer)) ? $star+1 : false;
                if ($pos === false)
                    break;
            } //while
        }
        
        /**
        *  function _removeKeywords()
        *
        *  Removes keywords from the buffer. Keywords are detected as words
        *  divided from other text by word boundaries (characters except
        *  alphanumeric symbols and _)
        *  @param &$buffer - string to be processed
        */
        private function _removeKeywords(&$buffer) {
            //remove the asterisk
            $this->_removeAsterisk($buffer);
            
            //remove keywords (some removed with "non-operator" asterisk)
            foreach ($this->keywords as $keyword) {
                $keyword = "/\b" . preg_quote($keyword, "/") . "\b/";
                $buffer = preg_replace($keyword, "", $buffer);
            }
        }
         
        /**
        *  function _removeKeywordsHandler()
        *
        *  Calls _removeKeywords() for lines separated by newline character or ;
        *  @param &$buffer - string to be processed
        */    
        private function _removeKeywordsHandler(&$buffer) {
            $sem = mb_strpos($buffer,";",0); //separate lines
            if ($sem !== false){
                $buf = mb_substr($buffer,0,$sem);
                $b = mb_substr($buffer,$sem+1);
                $this->_removeKeywordsHandler($buf);
                $this->_removeKeywordsHandler($b);
                $buffer = $buf. $b;
            }
            else            
                $this->_removeKeywords($buffer);             
        }
        
        /**
        *  function _removeConstants()
        *
        *  Removes all occurences of float and integer constants according to
        *  ISO C norm (sections 6.4.4.1 and 6.4.4.2).
        *  This includes following notations: 0726, 4561L, 2143U, 0x45AEFll
        *  for integer and 0.2, 4.2e+13, 12.47561f, 2E-5, 0.42, 1. , 0x0.BAP+2L
        *  for float.
        *
        *  @param &$buffer - string to be processed
        */
        private function _removeConstants(&$buffer) {
            $fract_const = "([0-9]*(\.[0-9]+)|\b[0-9]+\.)";
            $exp = "([eE][+-]?[0-9]+)";
            $float_suf = "[flFL]";
            $dec_float = "(" . $fract_const . $exp . "?" . $float_suf . "?"
                             . "|[0-9]+" . $exp.$float_suf . "?".
                         ")";
            
            $hex_fract_const = "([0-9a-fA-F]*(\.[0-9a-fA-F]+)|[0-9a-fA-F]+\.)";             
            $binary_exp = "[pP][+-]?[0-9]+";
            $hex_float = "(0[xX]" . $hex_fract_const . $binary_exp . $float_suf . "?".
                          "|0[xX][0-9a-fA-F]+" . $binary_exp . $float_suf ."?" . 
                         ")";
                                   
            $float = $dec_float . "|" . $hex_float;
            
            $dec = "[0-9]*[uU]?[lL]?[lL]?"; 
            $hex = "0[xX][0-9a-fA-F]+[uU]?[lL]?[lL]?";           
            $int = "\b" . $hex . "\b|\b". $dec . "\b";
            
            $num = $float . "|" . $int;
                        
            $pattern = "/" . $num . "/";
            $buffer = preg_replace($pattern, "", $buffer);  
        }
        
        /**
        *  function _removeMisc()
        *  
        *  Removes miscellaneous string patterns from the $buffer.
        *  This means several punctuators and square brackets with their content.
        *
        *  @param &$buffer - string to be processed
        */
        private function _removeMisc(&$buffer) {
            $str = array("...","<:",":>","<%","%>","%:","%:%:"); //punctuators, etc.
            foreach ($str as $s) {                
                while (($pos = mb_strpos($buffer,$s)) !== false) {
                    $buffer = mb_substr($buffer,0,$pos)." ".mb_substr($buffer,$pos+mb_strlen($s),mb_strlen($buffer));
                }
            }
            
            $buffer = preg_replace("/\[.*\]/", "", $buffer); // IND not implemented
        } 
     
        /**
        *  function _countOperators()
        *
        *  Counts every occurence of the each one ISO C simple operator.
        *
        *  @param &$buffer - string to be processed
        *  @param &$chars - number of characters 
        */
        private function _countOperators(&$buffer,&$chars) {
            foreach ($this->operators as $op) {            
                while (($pos = mb_strpos($buffer,$op)) !== false) {
                    $chars++;
                    $buffer = mb_substr($buffer,0,$pos)." ".mb_substr($buffer,$pos+mb_strlen($op),mb_strlen($buffer));
                }                
            }
        }       
        
        /**
        *  function _countKeyWords()
        *
        *  Counts every occurence of the each one ISO C keyword inside non-word
        *  boundaries (so identificators like ifelsethenbegin are not taken).
        *
        *  @param &$buffer - string to be processed
        *  @param &$chars - number of characters  
        */
        private function _countKeyWords(&$buffer,&$chars) {
            foreach ($this->keywords as $key) {            
                 $key = "/\b".preg_quote($key, "/")."\b/";
                 $chars+= preg_match_all($key,$buffer,$out);    
            }
        }            
        
        /**
        *  function _removeStringLiterals()
        *
        *  Removes string literals from the $buffer, ie. 'a', '0', '\t', etc.
        *
        *  @param &$buffer - string to be processed 
        */
        private function _removeStringLiterals(&$buffer) {
            while (($pos = mb_strpos($buffer,"'")) !== false) {
                $end = mb_strpos($buffer,"'",$pos);
                $buffer = mb_substr($buffer,0,$pos-1)." ".mb_substr($buffer,$end+1,mb_strlen($buffer));
            }
        }
        
        /**
        *  function _removeOperators()
        *
        *  Removes ISO C operators from the $buffer.
        *
        *  @param &$buffer - string to be processed
        */
        private function _removeOperators(&$buffer) {
            foreach ($this->operators as $op) {                
                while (($pos = mb_strpos($buffer,$op)) !== false) {
                    $buffer = mb_substr($buffer,0,$pos)." ".mb_substr($buffer,$pos+mb_strlen($op),mb_strlen($buffer));
                }
            }
        }
        
        /**
        *  function _countIdentificators()
        *
        *  Counts number of words made of alfanumeric symbols and _ .
        *
        *  @param &$buffer - string to be processed
        *  @param &$chars - number of characters  
        */
        private function _countIdentificators(&$buffer,&$chars) {
            $arr = preg_match_all('/([A-Za-z0-9\_]+)/', $buffer,$out);
            $chars += $arr;
        }
        
        /**
        *  function _getFileStats()
        *
        *  Core method of Stats. Proceeds according to arguments the main script
        *  has been run with. For parameter -w a search of following string pattern
        *  in whole file is conducted. For other parameters $file is read 
        *  line-by-line, lines of macros are skipped. For every -koic parameter
        *  necessary changes to the buffer are made (removing every string,
        *  every comment for -k) before the parameter core function is being called
        *  (such as _countKeyWords() for -k).        *
        *
        *  @param $file - file to be processed (string)
        *  @return $file - processed file (string)
        *  @return $chars - number of occurences
        */
        private function _getFileStats($file) {
            $fin = openFileInput($file);
                                           
            $chars = 0; //1 parameter at time=>one counter  
            if ($this->par->search()) {
                $par = $this->par->pattern();
                $chars = $this->_searchFile($par,$file);
            }
            else {                  
                $isMacro = false;         
                while(($buffer = fgets($fin,4096)) !== false) {
                    $buffer = trim($buffer,"\r"); // \n should be same as \r\n
                
                    /*skips line of macro*/
                    if ($isMacro //previous line stated multiline macro
                        || (preg_match(startMacro,$buffer)
                             && !$this->isComment
                             && !$this->isLineComment
                             && !$this->isString)) {
                             
                                $isMacro = preg_match(multiLine,$buffer);
                                continue;                             
                    }         
                    if ($this->par->keywords()) {
                        $this->_removeStringsHandler($buffer);
                        $this->_removeComments($buffer);                        
                        $this->_countKeyWords($buffer,$chars);
                    }                  
                    if ($this->par->identificators()) {
                        $this->_removeStringsHandler($buffer);
                        $this->_removeComments($buffer);  
                        $this->_removeKeywordsHandler($buffer);
                        $this->_removeMisc($buffer);
                        $this->_removeConstants($buffer);
                        $this->_removeStringLiterals($buffer);
                        $this->_removeOperators($buffer);
                        $this->_countIdentificators($buffer,$chars);
                    }                    
                    if ($this->par->operators()) {
                        $this->_removeStringsHandler($buffer);
                        $this->_removeComments($buffer);
                        $this->_removeKeywordsHandler($buffer);
                        $this->_removeMisc($buffer);
                        $this->_removeConstants($buffer);
                        $this->_countOperators($buffer,$chars);
                    }
                    if ($this->par->charsInComments()) {
                        $this->_removeStringsHandler($buffer);
                        $chars = $this->_parComments($buffer, $chars);
                    }
                }  
                      
                if (!feof($fin)) {
                    fclose($fin);
                    printError("Selhalo cteni ze vstupniho souboru.",errInput);
                }
                fclose($fin);
            }
            
            if ($this->par->path()) {                
                $pos = mb_strrpos($file,"/");
                $file = mb_substr($file,$pos+1,mb_strlen($file)); 
            }
            
            return array($file,$chars);
        }
        
        /**
        *  function getFileStats()
        *  
        *  _getFileStats() handler. For each file in the array the _getFileStats()
        *  is launched. Return values are stored inside $this->stats array while
        *  $this->chars contains total number of occurences.
        */
        public function getFileStats() {
            foreach($this->files as $file) {
                $ret = $this->_getFileStats($file);            
                $this->chars += $ret[1];
                array_push($this->stats,$ret);
            }            
        }
    }
?>
