function onCookies()
{
        $(document).ready(function(){
        var hid1 = getCookie("hidden1");
        var hid2 = getCookie("hidden2");
        $("#hidden1").hide();
        $("#hidden2").hide();
        
        if (hid1 == "true") {
            $("#hidden1").show();
        }   
         
        if (hid2 == "true") {
            $("#hidden2").show();
        }    
        
        $("#hid1").click(function() {
            if (document.getElementById("hidden1").style.display == 'none') {
                $("#hidden1").show();  
                setCookie("hidden1","true");
            }
            else {
                setCookie("hidden1","false");
                $("#hidden1").hide();
            }
        });
        $("#hid2").click(function() {
            if (document.getElementById("hidden2").style.display == 'none') {
                setCookie("hidden2","true");
                $("#hidden2").show();
            }
            else {
                setCookie("hidden2","false");
                $("#hidden2").hide();
            }
        });
    });
}

function getCookie(cname)
{    
    var cookies = document.cookie.split(";");
    var name = cname + "=";
    for(var i=0; i<cookies.length; i++) 
    {
        var c = cookies[i].trim();
        if (c.indexOf(name) == 0 ) return c.substring(name.length,c.length);
    }
    return "";
}

function setCookie(item,value)
{    
    var d = new Date();
    d.setTime(d.getTime() + 1000 * 120);
    document.cookie = item + "=" + value + "; EXPIRES="+d.toGMTString();
}
