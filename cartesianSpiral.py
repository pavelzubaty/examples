# Spiral in Cartesian space, starting with (0, 0), limited to (20000, 20000)

def solution(X, Y):
    # Python 2.7
    quadrant = 0
    n = 0 #number of points in the current line
    i = 0 #index in one current line
    xx = 0; yy = 0
    cnt = 0

    if (X < -20000 or X > 20000 \
        or Y < -20000 or Y > 20000):
            return -1
    
    while(xx != X or yy != Y):
        
        if (quadrant == 0):            
            yy = yy + 1
            i += 1
            if (i > n):
                quadrant = 1
                i = 0
        elif (quadrant == 1):
            xx = xx + 1
            i += 1
            if (i > n):
                quadrant = 2
                i = 0
                n += 1
        elif (quadrant == 2):
            yy = yy - 1
            i += 1
            if (i > n):
                quadrant = 3
                i = 0
        else: #(quadrant == 3):
            xx = xx - 1
            i += 1
            if (i > n):
                quadrant = 0
                i = 0
                n += 1        
        cnt += 1
        #print (xx,yy)
        
    return cnt
