#!/usr/bin/env ksh

function HELP 
 (
   echo -e "findproc prints PID of every process that conforms to specific criteria"
   echo -e "usage: findproc criterion1 [criterion2 [...]]"
   echo -e "criteria:"
   echo -e "    -n regexp      the process name matches regexp"
   echo -e "    -a regexp      an argument of a process matches regexp"
   echo -e "    -c regexp      the current working directory matches regexp"
   echo -e "    -x file        the process executable file is the same as file"
 )

System=$(uname) #zjistim, jestli se jedna o FreeBSD, Linux nebo nepodporovany system

mark=0 #protoze merlin asi bere :) jako joke...
while getopts :n:a:c:x: opt
 do
   case $opt in
       n) n_file="$OPTARG" #zkratka z "the name of the file"
          mark=1
          ;;
       a) arg_str="$OPTARG"
          mark=1
          ;;
       c) cur_dir="$OPTARG"
          mark=1
          ;;
       x) x_file="$OPTARG"
          mark=1
          ;;
      \?) HELP; exit 1;;
       :) HELP; exit 1;;
   esac
 done
shift $(($OPTIND-1))

if [ $mark -eq 0 ]; then
 HELP; exit 1;
fi

if [ $System = "Linux" ]; then #nebudu se preci ptat pri kazdem PID na OS
 for PID in /proc/[0-9]*/cmdline; do
  PID=`echo $PID| cut -d'/' -f3` # PID vybrany pomoci cut

  if [ $n_file ]; then #pak nactu prvni radek urcujici jmeno procesu
      if ! `strings -n 1 2>/dev/null < /proc/$PID/cmdline | head -n 1| grep -q "$n_file"`
       then
         continue #kriterium nebylo splneno, bere dalsi PID - plati pro vs. continue
      fi
  fi

  if [ $arg_str ]; then #a nactu parametry
      if ! `strings -n 1 2>/dev/null < /proc/$PID/cmdline | tail -n +2 | grep -q -w "$arg_str"`
        then
         continue      
      fi
  fi
  
  if [ $cur_dir ]; then #pokud 9. sloupec vypsaneho radku neodpovida adrese... tak sebere dalsi PID a opakuje cyklus
      if ! `lsof -w -d cwd -a -p $PID| awk {'print$9'} | grep -q "$cur_dir"` #nejdriv jsem daval cut misto awku, ale stejne nefungoval
       then
        continue
      fi
  fi
  
  if [ $x_file ]; then
      tst_file=`readlink /proc/$PID/exe 2>/dev/null` #vrati soubor programu ze slozky dane PIDem      
      if [ "$x_file" != "$tst_file" ]
       then
        continue
      fi
  fi
  echo $PID
 done
elif [ $System = "FreeBSD" ]; then
 for PID in /proc/[0-9]*/cmdline; do
  PID=`echo $PID| cut -d'/' -f3` # PID vybrany pomoci cut

  if [ $n_file ]; then #pak nactu prvni radek urcujici jmeno procesu
      if ! `strings -n 1 2>/dev/null < /proc/$PID/cmdline | head -n 1| grep -q "$n_file"`
       then
         continue #kriterium nebylo splneno, bere dalsi PID - plati pro vs. continue
      fi
  fi

  if [ $arg_str ]; then #a nactu parametry
      if ! `strings -n 1 2>/dev/null < /proc/$PID/cmdline | tail -n +2 | grep -q -w "$arg_str"`
        then
         continue      
      fi
  fi
  
  if [ $cur_dir ]; then
      if ! `procstat -f $PID 2>/dev/null| awk {'print$10'} | grep -q "$cur_dir"` #pokud 10. sloupec vypsaneho radku neodpovida adrese...
        then
          continue
      fi
  fi
  
  if [ $x_file ]; then
	    tst_file=`readlink /proc/$PID/file 2>/dev/null` #vrati soubor programu ze slozky dane PIDem
      
      if [ "$x_file" != "$tst_file" ]
       then 
        continue
      fi
  fi
  echo $PID
 done
 
else
 echo "Nepodporovany system">&2; exit 1;
fi
  
exit 0
